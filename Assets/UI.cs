﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Google;
using HotPlay;
using System;

public class UI : MonoBehaviour
{
    public void OnClickHotNow()
    {
        LoginManager.Instance.ShowLogin(Success, Fail, true);
    }

    public void OnClickClearPref()
    {
        Debug.Log("Clear all pref");
        PlayerPrefs.DeleteAll();
    }

    private void Awake()
    {
        string registerInstallationURL = "http://k8s-hope-dev.trueaxion.games:8080/api/register-installation?client_version=0.0.1";
        string baseURL = "https://alpha.accura.hot-now.com";
        string language = "en";

        LoginManager.Instance.InitManager(registerInstallURL: registerInstallationURL, baseBackendURL: baseURL, selectedLang: language,
            (isSuccess) =>
            {
                OnInitialized(isSuccess, isSuccess ? null : "Hotnow login fail.");
            });
    }

    private void OnInitialized(bool isSuccess, string v)
    {
        Debug.Log($"INIT: {isSuccess}|{v}");
    }

    private void Fail()
    {
        Debug.Log($"Hot fail");
    }

    private void Success()
    {
        Debug.Log($"Hot success");
    }

    public void Google()
    {
        var t = GoogleSignIn.DefaultInstance.SignIn();

        t.ContinueWith(task =>
        {
            if (task.IsCanceled)
            {
                Debug.Log($"IsCanceled");
            }
            else if (task.IsFaulted)
            {
                Debug.Log($"IsFaulted");
            }
            else
            {
                Debug.Log($"else");

            }
        });
    }
}
