﻿#if UNITY_IOS
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.iOS.Xcode;
using System.IO;
using UnityEngine;
using System;
using UnityEditor.Build;

public class BuildPostProcess
{
    [PostProcessBuild]
    public static void OnPostprocessBuild(BuildTarget buildTarget, string path)
    {
        Debug.Log("<color=#00ff1d><b> POST PROCESS STARTED!</b></color>");
        if (buildTarget == BuildTarget.iOS)
        {
            string projPath = path + "/Unity-iPhone.xcodeproj/project.pbxproj";
            PBXProject proj = new PBXProject();
            proj.ReadFromString(File.ReadAllText(projPath));
#if UNITY_2019_1_OR_NEWER
            string target = proj.GetUnityMainTargetGuid();
#else
            string target = proj.TargetGuidByName("Unity-iPhone");
#endif
            Debug.Log("<color=yellow><b> POST PROCESS: add settings bundle </b></color>");
            AddSettingsBundle(path, proj, target);
            Debug.Log("<color=yellow><b> POST PROCESS: update info plist </b></color>");
            UpdateInfoPlist(path, proj, target);
            Debug.Log("<color=#00ff1d><b> POST PROCESS DONE!</b></color>");
        }
    }

#if UNITY_2019_3_OR_NEWER
    //Fix for facebook build error in xcode in version 2019.3
    [PostProcessBuild(99)]
    public static void BeforeFacebookOnPostProcessBuild(BuildTarget target, string pathToBuiltProject)
    {
        Debug.Log("<color=#00ff1d><b>FACEBOOK POST PROCESS STARTED!</b></color>");
        if (target != BuildTarget.iOS)
        {
            return;
        }

        var fullPath = Path.Combine(pathToBuiltProject, Path.Combine("Libraries", "RegisterMonoModules.h"));
        if (!File.Exists(fullPath))
        {
            File.Create(fullPath).Close();
        }
        Debug.Log("<color=#00ff1d><b>FACEBOOK POST PROCESS DONE!</b></color>");
    }
#endif


    /// <summary>
    /// /Adds the settings bundle to the build 
    /// </summary>
    internal static void AddSettingsBundle(string path, PBXProject proj, string target)
    {
        var fileName = $"GoogleService-Info.plist";
        var folderName = $"/IGA/Login/Editor/IOSGoogleInfoPlist/";
        var fileToCopy = $"{Application.dataPath}{folderName}{fileName}";

        if (File.Exists(fileToCopy))
        {
            File.Copy(fileToCopy, Path.Combine(path, fileName));
            var guid = proj.AddFile(fileName, fileName, PBXSourceTree.Source);
            proj.AddFileToBuild(target, guid);
            File.WriteAllText(path + "/Unity-iPhone.xcodeproj/project.pbxproj", proj.WriteToString());
        }
        else
        {
            throw new BuildFailedException("Cannot find GoogleService-Info.plist in Assets/IGA/Login/Editor/IOSGoogleInfoPlist");
        }
    }

    /// <summary>
    /// Updates the info plist.
    /// </summary>
    static void UpdateInfoPlist(string path, PBXProject proj, string target)
    {
        // Get plist
        string plistPath = path + "/Info.plist";
        PlistDocument plist = new PlistDocument();
        plist.ReadFromString(File.ReadAllText(plistPath));

        // Get root
        PlistElementDict rootDict = plist.root;

        //add url types
        var urlTypes = plist.root["CFBundleURLTypes"];
        PlistElementArray cfBundleURLTypes;
        if (urlTypes == null)
        {
            cfBundleURLTypes = plist.root.CreateArray("CFBundleURLTypes");
        }
        else
        {
            cfBundleURLTypes = urlTypes.AsArray();
        }
        var dict = cfBundleURLTypes.AddDict();
        dict.SetString("CFBundleTypeRole", "Editor");
        dict.SetString("CFBundleURLName", "");
        var arr = dict.CreateArray("CFBundleURLSchemes");
        arr.AddString(LoadGoogleIOSAppIDFromConfig());
        File.WriteAllText(plistPath, plist.WriteToString());
    }

    /// <summary>
    /// Copies the and replace directory
    /// </summary>
    /// <param name="srcPath">Source path.</param>
    /// <param name="dstPath">Dst path.</param>
    internal static void CopyAndReplaceDirectory(string srcPath, string dstPath)
    {
        if (Directory.Exists(dstPath))
        {
            Directory.Delete(dstPath, true);
        }

        if (File.Exists(dstPath))
        {
            File.Delete(dstPath);
        }

        Directory.CreateDirectory(dstPath);

        foreach (var file in Directory.GetFiles(srcPath))
            File.Copy(file, Path.Combine(dstPath, Path.GetFileName(file)));

        foreach (var dir in Directory.GetDirectories(srcPath))
            CopyAndReplaceDirectory(dir, Path.Combine(dstPath, Path.GetFileName(dir)));
    }

    internal static string LoadGoogleIOSAppIDFromConfig(bool isReverse = true)
    {
        string path = $"Assets/IGA/Login/Config/LoginConfig.asset";

        var config = AssetDatabase.LoadAssetAtPath<LoginConfig>(path);
        if (config != null)
        {
            Debug.Log("LoginConfig != null");
            var webClientId = config.webClientId_IOS;

            if (isReverse)
            {
                if (webClientId.StartsWith("com", StringComparison.CurrentCulture))
                {
                    return webClientId;
                }
                else
                {
                    //Reverse xxxxxxxxxx.app.googleusercontent.com to com.googleusercontent.app.xxxxxxxx
                    var split_clientId = webClientId.Split('.');
                    Array.Reverse(split_clientId);
                    return String.Join(".", split_clientId);
                }
            }
            else
            {
                return webClientId;
            }
        }
        throw new BuildFailedException("Cannot find LoginConfig");
    }
}

#endif