﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace HotPlay.UI
{
    public class GenericLoginPanel : MonoBehaviour
    {
        public event Action OnClickHotNowLoginButton;
        public event Action OnClickFacebookLoginButton;
        public event Action OnClickGoogleLoginButton;
        public event Action OnClickSkipToLoginButton;

        public event Action EvLoginSuccess;

        [FormerlySerializedAs("HotNowLoginButton")] [SerializeField] private Button hotNowLoginButton;
        [FormerlySerializedAs("FacebookLoginButton")] [SerializeField] private Button facebookLoginButton;
        [FormerlySerializedAs("GoogleLoginButton")] [SerializeField] private Button googleLoginButton;
        [FormerlySerializedAs("SkipLoginButton")] [SerializeField] private Button skipLoginButton;

        [SerializeField] private LoadingFade _loadingFade;

        private void Awake()
        {
            OnSetButtonEventAction();
            OnAddEventAction();
        }

        private void OnSetButtonEventAction()
        {
            this.AddButtonListener(hotNowLoginButton, OnClickHotNowLoginButton);
            this.AddButtonListener(facebookLoginButton, OnClickFacebookLoginButton);
            this.AddButtonListener(googleLoginButton, OnClickGoogleLoginButton);
            this.AddButtonListener(skipLoginButton, OnClickSkipToLoginButton);
        }

        private void OnAddEventAction()
        {
            _loadingFade.EvSuccessShown += _loadingFade_EvSuccessShown;
        }

        void _loadingFade_EvSuccessShown()
        {
            EvLoginSuccess?.Invoke();
        }

        private void AddButtonListener(Button button, Action action)
        {
            button.onClick.AddListener(() =>
            {
                action?.Invoke();
            });
        }
    }
}
