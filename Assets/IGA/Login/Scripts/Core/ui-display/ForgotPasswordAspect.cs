﻿using System;
using System.Text.RegularExpressions;
using HotPlay.localization;
using UnityEngine;
using UnityEngine.UI;

namespace HotPlay.UI
{
    public class ForgotPasswordAspect : MonoBehaviour
    {
        public event Action<string> EvForgotPassword;
        public event Action EvForgotPasswordSuccess;

        [SerializeField] private InputField _emailInput;
        [SerializeField] private Button _remindButton;
        [SerializeField] private LoadingFade _loadingFade;
        [SerializeField] private Text _errorMessage;
        [SerializeField] private GameObject processPanel;
        [SerializeField] private GameObject successPanel;

        public void Show()
        {
            gameObject.SetActive(true);
            ShowProcessForgotPanel();
            Reset();
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        public void OnReminderFailed(string message)
        {
            _loadingFade.HideLoadingFade();
            ShowErrorMessage(Localization.Instance.Get("error_forgot_common"));
        }

        public void OnReminderDone(string message)
        {
            _loadingFade.ShowSuccess(1f);
        }

        private void Awake()
        {
            OnAddButtonListener();
            OnAddEventAction();

            Reset();
        }

        private void OnAddButtonListener()
        {
            AddButtonListener(_remindButton, OnRemindButton);
        }

        private void OnAddEventAction()
        {
            _loadingFade.EvSuccessShown += (() =>
            {
                _loadingFade.HideLoadingFade();
                ShowSuccessForgotPanel();
            });
        }

        private void Reset()
        {
            _emailInput.text = "";
            HideErrorMessage();
            _loadingFade.HideLoadingFade();
        }

        private void OnRemindButton()
        {
            if (Application.internetReachability == NetworkReachability.NotReachable)
            {
                ShowErrorMessage("Cannot resolve destination host");
                return;
            }

            string email = _emailInput.text;

            Debug.Log(email);

            if (string.IsNullOrEmpty(email))
            {
                ShowErrorMessage(Localization.Instance.Get("error_login_different_method"));
            }
            else if (!Regex.IsMatch(email, @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"))
            {
                ShowErrorMessage(Localization.Instance.Get("error_forgot_invalidemail"));
            }
            else
            {
                _loadingFade.ShowWait();
                EvForgotPassword(email);
            }
        }

        #region Error Message

        private void ShowErrorMessage(string msg)
        {
            if (msg == "Cannot resolve destination host")
            {
                msg = Localization.Instance.Get("error_nointernet");
            }
            _errorMessage.text = msg;
            _errorMessage.gameObject.SetActive(true);
        }

        private void HideErrorMessage()
        {
            _errorMessage.gameObject.SetActive(false);
        }

        #endregion //Error Message

        private void ShowProcessForgotPanel()
        {
            processPanel.SetActive(true);
            successPanel.SetActive(false);
        }

        private void ShowSuccessForgotPanel()
        {
            processPanel.SetActive(false);
            successPanel.SetActive(true);
        }

        private void AddButtonListener(Button button, Action action)
        {
            button.onClick.AddListener(() =>
            {
                action?.Invoke();
            });
        }
    }
}