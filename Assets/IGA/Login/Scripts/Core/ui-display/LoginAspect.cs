﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.ComponentModel;
using HotPlay.Internal;
using HotPlay.localization;

namespace HotPlay.UI
{
    public class LoginAspect : MonoBehaviour
    {
        public event Action<string, string> EvLogin;
        public event Action EvChangeToRegisterPanel;
        public event Action EvChangeToForgotPasswordPanel;
        public event Action EvLoginSuccess;

        [SerializeField] private InputField _loginInput;
        [SerializeField] private InputField _passwordInput;
        [SerializeField] private Text _errorMessage;
        [SerializeField] private LoadingFade _loadingFade;

        //Button
        [SerializeField] private Button loginButton;
        [SerializeField] private Button toRegisterPanelButton;
        [SerializeField] private Button toForgotPassPanelButton;

        public void Show()
        {
            gameObject.SetActive(true);
            Reset();
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        private void Awake()
        {
            OnAddButtonListener();
            OnAddEventAction();

            Reset();
        }

        private void OnAddButtonListener()
        {
            AddButtonListener(loginButton, OnLoginButton);
            AddButtonListener(toRegisterPanelButton, EvChangeToRegisterPanel);
            AddButtonListener(toForgotPassPanelButton, EvChangeToForgotPasswordPanel);
        }

        private void OnAddEventAction()
        {
        }

        private void Reset()
        {
            _loginInput.text = "";
            _passwordInput.text = "";
            HideErrorMessage();
            _loadingFade.HideLoadingFade();
        }

        private void OnLoginButton()
        {
            if (Application.internetReachability == NetworkReachability.NotReachable)
            {
                ShowErrorMessage("Cannot resolve destination host");
                return;
            }

            string login = _loginInput.text;
            string pass = _passwordInput.text;

            if (string.IsNullOrEmpty(login) ||
                string.IsNullOrEmpty(pass) ||
                pass.Length < 8)
            {
                ShowErrorMessage(Localization.Instance.Get("error_login_incorrectdata"));
            }
            else if (login.Length < 6)
            {
                ShowErrorMessage("shortName");
                return;
            }
            else if (login.Contains(" ") || pass.Contains(" "))
            {
                ShowErrorMessage("whitespaces");
                return;
            }

            else
            {
                OnWaitingProcess();
                EvLogin(login, pass);
            }
        }

        public void OnWaitingProcess()
        {
            _loadingFade.ShowWait();
        }

        public void OnLoginFailed(string message)
        {
            _loadingFade.HideLoadingFade();
            ShowErrorMessage(message);
            _passwordInput.text = "";
        }

        public void OnLoginSuccess(string message)
        {
            _loadingFade.ShowSuccess(1.5f);
            HideErrorMessage();
            _passwordInput.text = "";
        }

        #region Error Message

        private void ShowErrorMessage(string message)
        {
            Debug.Log("ShowErrorMessage  " + message);
            if (message == "Cannot resolve destination host")
            {
                _errorMessage.text = Localization.Instance.Get("error_nointernet");
                _errorMessage.gameObject.SetActive(true);
                _errorMessage.SetAllDirty();
                return;
            }
            else if (message == ErrorType.LoginErrorByCancel.ToString())
            {
                _errorMessage.text = string.Empty;
                _errorMessage.gameObject.SetActive(false);
                _errorMessage.SetAllDirty();
                return;
            }
            else if (message == "HTTP/1.1 422 Unprocessable Entity")
            {
                string loginmethods = null;
                if (IGA_LoginInterface.LastUsedLoginMethod == "hotnow")
                {
                    loginmethods = Localization.Instance.Get("error_login_fb_g");
                }
                else if (IGA_LoginInterface.LastUsedLoginMethod == "facebook")
                {
                    loginmethods = Localization.Instance.Get("error_login_hn_g");
                }
                else if (IGA_LoginInterface.LastUsedLoginMethod == "google")
                {
                    loginmethods = Localization.Instance.Get("error_login_hn_fb");
                }

                _errorMessage.text = string.Format(
                    Localization.Instance.Get("error_login_different_method"),
                    loginmethods
                );

                _errorMessage.gameObject.SetActive(true);
                _errorMessage.SetAllDirty();
                return;
            }
            else if (message == "shortName")
            {
                _errorMessage.text = Localization.Instance.Get("error_register_shortname");
                _errorMessage.gameObject.SetActive(true);
                _errorMessage.SetAllDirty();
                return;
            }
            else if (message == "whitespaces")
            {
                _errorMessage.text = Localization.Instance.Get("error_register_whitespaces");
                _errorMessage.gameObject.SetActive(true);
                _errorMessage.SetAllDirty();
                return;
            }
            else
            {
                _errorMessage.text = Localization.Instance.Get("error_login_incorrectdata");
                _errorMessage.gameObject.SetActive(true);
                _errorMessage.SetAllDirty();
                return;
            }
        }

        private void HideErrorMessage()
        {
            _errorMessage.gameObject.SetActive(false);
        }

        #endregion //Error Message

        #region Loader

        void _loadingFade_EvSuccessShown()
        {
            EvLoginSuccess?.Invoke();
        }

        #endregion //Error Message      

        private void AddButtonListener(Button button, Action action)
        {
            button.onClick.AddListener(() =>
            {
                action?.Invoke();
            });
        }
    }
}
