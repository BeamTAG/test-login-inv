﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;
using HotPlay.localization;

namespace HotPlay.UI
{
    public class RegisterAspect : MonoBehaviour
    {
        public event Action<string, string, string, string> EvRegister;
        public event Action EvBack;
        public event Action EvRegisterSuccess;

        [SerializeField] private InputField _nameInput;
        [SerializeField] private InputField _usernameInput;
        [SerializeField] private InputField _passwordInput;
        [SerializeField] private InputField _passwordRepeatInput;
        [SerializeField] private InputField _emailInput;

        [SerializeField] private Button _registerButton;
        [SerializeField] private Button _backButton;

        [SerializeField] private LoadingFade _loadingFade;

        [SerializeField] private Text _errorMessage;

        public void Show()
        {
            gameObject.SetActive(true);

            Reset();
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        public void OnRegisterFailed(string message)
        {
            HideLoadingFade();
            ShowErrorMessage(Localization.Instance.Get("error_register_common"));
            _passwordInput.text = "";
        }

        public void OnRegisterDone(string message)
        {
            _loadingFade.ShowSuccess(1.5f);
        }

        private void Awake()
        {
            OnAddButtonListener();
            OnAddEventAction();

            Reset();
        }

        private void OnAddButtonListener()
        {
            AddButtonListener(_backButton, EvBack);
            AddButtonListener(_registerButton, OnRegisterButton);
        }

        private void OnAddEventAction()
        {
            _loadingFade.EvSuccessShown += _loadingFade_EvSuccessShown;
        }

        private void Reset()
        {
            _nameInput.text = "";
            _usernameInput.text = "";
            _passwordInput.text = "";
            _passwordRepeatInput.text = "";
            _emailInput.text = "";

            HideErrorMessage();
            HideLoadingFade();
        }

        private void OnRegisterButton()
        {
            if (Application.internetReachability == NetworkReachability.NotReachable)
            {
                ShowErrorMessage("Cannot resolve destination host");
                return;
            }

            string fullName = _nameInput.text;
            string userName = _usernameInput.text;
            string pass = _passwordInput.text;
            string passRepeat = _passwordRepeatInput.text;
            string email = _emailInput.text;

            if (string.IsNullOrEmpty(fullName) ||
                string.IsNullOrEmpty(userName) ||
                string.IsNullOrEmpty(pass) ||
                string.IsNullOrEmpty(passRepeat) ||
                string.IsNullOrEmpty(email)
               )
            {
                ShowErrorMessage(Localization.Instance.Get("error_register_no_info"));
            }
            else if (pass.Length < 8)
            {
                ShowErrorMessage(Localization.Instance.Get("error_register_password"));
            }
            else if (pass != passRepeat)
            {
                ShowErrorMessage(Localization.Instance.Get("error_register_passwordnotmatch"));
            }
            else if (!Regex.IsMatch(email, @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"))
            {
                ShowErrorMessage(Localization.Instance.Get("error_register_invalid_email"));
            }
            else if (userName.Contains(" ") || pass.Contains(" "))
            {
                ShowErrorMessage(Localization.Instance.Get("error_register_whitespaces"));
                return;
            }
            else if (userName.Length < 6)
            {
                ShowErrorMessage(Localization.Instance.Get("error_register_shortname"));
                return;
            }
            else
            {
                ShowLoadingFade();
                EvRegister(userName, pass, fullName, email);
            }
        }

        #region Error Message

        private void ShowErrorMessage(string message)
        {
            if (message == "Cannot resolve destination host")
            {
                message = Localization.Instance.Get("error_nointernet");
            }

            _errorMessage.text = message;
            _errorMessage.gameObject.SetActive(true);
        }

        private void HideErrorMessage()
        {
            _errorMessage.gameObject.SetActive(false);
        }

        #endregion //Error Message

        #region Loader

        private void ShowLoadingFade()
        {
            _loadingFade.gameObject.SetActive(true);
        }

        private void HideLoadingFade()
        {
            _loadingFade.gameObject.SetActive(false);
        }

        void _loadingFade_EvSuccessShown()
        {
            EvRegisterSuccess?.Invoke();
        }

        #endregion //Error Message

        private void AddButtonListener(Button button, Action action)
        {
            button.onClick.AddListener(() =>
            {
                action?.Invoke();
            });
        }
    }
}