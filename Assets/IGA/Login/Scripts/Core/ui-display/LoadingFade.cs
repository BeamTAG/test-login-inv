﻿using UnityEngine;
using System;

namespace HotPlay.UI
{
    public class LoadingFade : MonoBehaviour
    {
        public event Action EvSuccessShown;

        [SerializeField] private GameObject _loader;
        [SerializeField] private GameObject _succesfullMarker;

        private float _timer;
        private float _timeLimit;

        private void OnEnable()
        {
            ShowWait();
        }

        public void ShowWait()
        {
            _loader.SetActive(true);
            _succesfullMarker.SetActive(false);

            gameObject.SetActive(true);
            _timer = 0;
            _timeLimit = -1;
        }

        public void ShowSuccess(float time)
        {
            _loader.SetActive(false);
            _succesfullMarker.SetActive(true);

            gameObject.SetActive(true);
            _timer = 0;
            _timeLimit = time;
        }

        public void HideLoadingFade()
        {
            _loader.SetActive(false);
            _succesfullMarker.SetActive(false);
            gameObject.SetActive(false);
        }

        private void Update()
        {
            _timer += Time.deltaTime;

            //rotate
            if (_loader.activeInHierarchy)
            {
                _loader.transform.Rotate(new Vector3(0, 0, 3));
            }


            //show success
            if (_succesfullMarker.activeInHierarchy)
            {
                if (_timer > _timeLimit && _timeLimit > 0)
                {
                    EvSuccessShown?.Invoke();
                    gameObject.SetActive(false);
                }
            }
        }

    }
}
