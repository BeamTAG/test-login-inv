﻿using UnityEngine;

[System.Serializable]
public class LoginConfig : ScriptableObject
{
    public string webClientId_IOS = "New MyScriptableObject";
    public string webClientId_Android = "New MyScriptableObject";
}