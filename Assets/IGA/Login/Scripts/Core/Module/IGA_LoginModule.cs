﻿using UnityEngine;
using HotPlay.UI;
using HotPlay.Internal.Token;

namespace HotPlay
{
    public class IGA_LoginModule : MonoBehaviour
    {
        [SerializeField] private LoginAspect _loginAspect;
        [SerializeField] private RegisterAspect _registerAspect;
        [SerializeField] private ForgotPasswordAspect _forgotPassAspect;
        [SerializeField] private GenericLoginPanel genericLoginPanel;

        private SimpleCallback _onLoginSuccess;
        private SimpleCallback _onLoginFailed;

        private TokenManager tokenManager = new TokenManager();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="onLoginSuccess"></param>
        /// <param name="onLoginFailed"></param>
        public void ShowLogin(SimpleCallback onLoginSuccess, SimpleCallback onLoginFailed)
        {
            ShowLogin();
            _onLoginFailed = onLoginFailed;
            _onLoginSuccess = onLoginSuccess;
        }

        private void ShowLogin()
        {
            gameObject.SetActive(true);

            //Show register firstly
            OnShowRegisterSubPanel();
        }

        private void HideLogin()
        {
            gameObject.SetActive(false);
        }

        #region Private Members

        private void Awake()
        {
            OnAddEventAction();
        }

        private void OnAddEventAction()
        {
            genericLoginPanel.OnClickHotNowLoginButton += OnShowLoginSubPanel;
            genericLoginPanel.OnClickGoogleLoginButton += OnActionLoginWithGoogle;
            genericLoginPanel.OnClickFacebookLoginButton += OnActionLoginWithFacebook;
            genericLoginPanel.OnClickSkipToLoginButton += _loginAspect_EvCloseLogin;
            genericLoginPanel.EvLoginSuccess += _loginAspect_EvCloseLogin;

            _loginAspect.EvLogin += _loginAspect_EvLogin;
            _loginAspect.EvChangeToRegisterPanel += OnShowRegisterSubPanel;
            _loginAspect.EvChangeToForgotPasswordPanel += OnShowForgotPasswordSubPanel;
            _loginAspect.EvLoginSuccess += _loginAspect_EvCloseLogin;

            _registerAspect.EvBack += OnShowLoginSubPanel;
            _registerAspect.EvRegister += _registerAspect_EvRegister;
            _registerAspect.EvRegisterSuccess += _loginAspect_EvCloseLogin;

            _forgotPassAspect.EvForgotPassword += _forgotPassAspect_EvForgotPassword;
            _forgotPassAspect.EvForgotPasswordSuccess += OnShowLoginSubPanel;
        }

        private void OnShowLoginSubPanel()
        {
            ShowLoginAspect();
            HideRegisterAspect();
            HideForgotPassAspect();
        }

        private void OnShowRegisterSubPanel()
        {
            HideLoginAspect();
            ShowRegisterAspect();
            HideForgotPassAspect();
        }

        private void OnShowForgotPasswordSubPanel()
        {
            HideLoginAspect();
            HideRegisterAspect();
            ShowForgotPassAspect();
        }

        #region ForgotPassword

        private void ShowForgotPassAspect()
        {
            _forgotPassAspect.Show();
        }

        private void HideForgotPassAspect()
        {
            _forgotPassAspect.Hide();
        }

        private void _forgotPassAspect_EvForgotPassword(string email)
        {
            IGA_LoginInterface.ForgotPasswordPassword(email, OnReminderSuccess, OnReminderFailed);
        }

        private void OnReminderSuccess(string msg)
        {
            _forgotPassAspect.OnReminderDone(msg);
        }

        private void OnReminderFailed(string msg)
        {
            _forgotPassAspect.OnReminderFailed(msg);
        }

        #endregion //ForgotPassword

        #region Register

        private void _registerAspect_EvRegister(string userName, string pass, string fullname, string email)
        {
            IGA_LoginInterface.Register(
                userName,
                pass,
                fullname,
                email,
                OnRegistrationSuccess,
                OnRegistrationFailed
            );
        }

        private void OnRegistrationSuccess(string message)
        {
            _registerAspect.OnRegisterDone(message);
        }

        private void OnRegistrationFailed(string message)
        {
            _registerAspect.OnRegisterFailed(message);
        }

        private void ShowRegisterAspect()
        {
            _registerAspect.Show();
        }

        private void HideRegisterAspect()
        {
            _registerAspect.Hide();
        }

        #endregion //Register

        #region Login

        private void ShowLoginAspect()
        {
            _loginAspect.Show();
        }

        private void HideLoginAspect()
        {
            _loginAspect.Hide();
        }

        private void _loginAspect_EvLogin(string login, string password)
        {
            IGA_LoginInterface.Login(
                    login,
                    password,
                    OnLoginSuccess,
                    OnLoginFailed
                );
        }

        private void OnActionLoginWithFacebook()
        {
            ShowLoginAspect();
            HideRegisterAspect();
            HideForgotPassAspect();

            _loginAspect.OnWaitingProcess();

            IGA_LoginInterface.LoginFacebook(
                  OnLoginSuccess,
                  OnLoginFailed
              );
        }

        private void OnActionLoginWithGoogle()
        {
            ShowLoginAspect();
            HideRegisterAspect();
            HideForgotPassAspect();

            _loginAspect.OnWaitingProcess();

            IGA_LoginInterface.LoginGoogle(
                 OnLoginSuccess,
                 OnLoginFailed
             );
        }

        private void OnLoginFailed(string message)
        {
            _loginAspect.OnLoginFailed(message);
        }

        private void OnLoginSuccess(string message)
        {
            _loginAspect.OnLoginSuccess(message);
        }

        private void _loginAspect_EvCancelLogin()
        {
            _onLoginFailed?.Invoke();
            HideLogin();
        }

        private void _loginAspect_EvCloseLogin()
        {
            _onLoginSuccess?.Invoke();
            tokenManager.SetLoggedInUser(tokenManager.GetUserToken());
            HideLogin();
        }

        #endregion //Login

        #endregion //  Private Members

    }
}