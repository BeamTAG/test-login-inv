﻿// #define DEBUG_REQUEST

using System;
using System.Collections;
using System.Collections.Generic;

using HotPlay.Internal.Requests;
using HotPlay.Internal.Token;

using UnityEngine;
using UnityEngine.Networking;

namespace HotPlay.Internal
{
    #region abstract members 

    /// <summary>
    /// abstract request
    /// </summary>
    internal abstract class GenericRequest
    {
        /// <summary>
        /// returns the url.
        /// </summary>
        internal string URL { get { return data == null ? null : data.URL; } }

        /// <summary>
        /// returns the method to be used
        /// </summary>
        internal string Method { get { return data == null ? null : data.Method; } }

        /// <summary>
        /// preseted request data.
        /// </summary>
        protected RequestData data;

        internal abstract void SetupRequest(RequestData data);
        internal abstract byte[] GetRequestData();
        internal abstract string GetRequestDataString();
        internal abstract void OnRequestDone(UnityWebRequest www);
    }

    /// <summary>
    /// bstract request with authentication.
    /// </summary>
    internal abstract class RequestWithAuthentication : GenericRequest
    {
        private Dictionary<string, string> _authHeaders;

        internal Dictionary<string, string> GetAuthHeaders()
        {
            return _authHeaders;
        }

        internal void SetAuthHeaders(Dictionary<string, string> headers)
        {
            _authHeaders = headers;
        }
    }

    /// <summary>
    /// Data that is needed to fill the request.
    /// </summary>
    internal abstract class RequestData
    {
        /// <summary>
        /// The type of the requst.
        /// </summary>
        internal RequstType RequstType;

        /// <summary>
        /// The URL to be called
        /// </summary>
        internal string URL;

        /// <summary>
        /// The method to be used
        /// </summary>
        internal string Method;
    }

    #endregion // abstract members 

    #region enums

    /// <summary>
    /// types of the requests.
    /// </summary>
    internal enum RequstType
    {
        Login = 1,
        Register = 2,
        Logout = 3,
        GetUserDetail = 4,
        ChangePassword = 5,
        ForgotPassword = 6,
        ResetPassword = 7,
        LoginFacebook = 8,
        LoginGoogle = 9,
        RegisterUserToken = 10,
        TokenRenewer = 11,
        // etc.
    }

    /// <summary>
    /// Error type.
    /// </summary>
    internal enum ErrorType
    {
        LoginErrorByCancel = 0,
    }

    #endregion // enums

    #region Constants

    internal class Constants
    {
        public static string BaseURL = "https://dev.hot-now.com/auth";
        public const string LoginSubURL = "/odin/app/api/login/";
        public const string RegisterSubURL = "/odin/app/register/";
        public const string LoginFacebookSubURL = "/odin/app/api/login/facebook/";
        public const string LoginGoogleSubURL = "/odin/app/api/login/google/";

        public const string LogoutSubURL = "/odin/api/logout/";
        public const string GetUserDetailsSubURL = "/odin/app/api/verify/";
        public const string ChangePasswordSubURL = "/odin/api/me/password/";
        public const string ForgotPasswordSubURL = "/odin/api/forgotten-password/";
        public const string TokenRenewerURL = "/odin/app/api/renew-jwt/";
        public static string RegisterUserTokenURL = "";
    }

    #endregion //Constants

    /// <summary>
    /// Request factory
    /// </summary>
    internal static class RequestFactory
    {
        internal static GenericRequest CreateRequest(RequestData data)
        {
            GenericRequest request = null;
            switch (data.RequstType)
            {
                case RequstType.Login:
                    {
                        request = new LoginRequest();
                        request.SetupRequest(data);
                        break;
                    }
                case RequstType.Register:
                    {
                        request = new RegisterRequest();
                        request.SetupRequest(data);
                        break;
                    }
                case RequstType.Logout:
                    {
                        request = new LogoutRequest();
                        request.SetupRequest(data);
                        break;
                    }
                case RequstType.GetUserDetail:
                    {
                        request = new GetUserDetailsRequest();
                        request.SetupRequest(data);
                        break;
                    }
                case RequstType.ChangePassword:
                    {
                        request = new ChangePasswordRequest();
                        request.SetupRequest(data);
                        break;
                    }
                case RequstType.ForgotPassword:
                    {
                        request = new ForgotPasswordRequest();
                        request.SetupRequest(data);
                        break;
                    }
                case RequstType.ResetPassword:
                    break;
                case RequstType.LoginFacebook:
                    {
                        request = new LoginFacebookRequest();
                        request.SetupRequest(data);
                        break;
                    }
                case RequstType.LoginGoogle:
                    {
                        request = new LoginGoogleRequest();
                        request.SetupRequest(data);
                        break;
                    }
                case RequstType.RegisterUserToken:
                    {
                        request = new RegisterUserTokenRequst();
                        request.SetupRequest(data);
                        break;
                    }
                case RequstType.TokenRenewer:
                    {
                        request = new TokenRenewerRequest();
                        request.SetupRequest(data);
                        break;
                    }
            }

            TokenManager tokenManager = new TokenManager();
            if (request is RequestWithAuthentication)
            {
                (request as RequestWithAuthentication).SetAuthHeaders(
                    new Dictionary<string, string>()
                    { { "Authorization", "Bearer " + tokenManager.GetUserToken () },
                    }
                );
                //Debug.Log("set header ::  Authorization - Bearer " + _authtoken);
            }

            return request;
        }
    }

    /// <summary>
    /// Request queue.
    /// </summary>
    internal class RequestQueue
    {
        private Queue<GenericRequest> _requestQueue;
        private GenericRequest _currentRequest;

        private TokenManager _tokenManager = new TokenManager();

        public void AddRequest(GenericRequest request)
        {
            if (_requestQueue == null)
            {
                _requestQueue = new Queue<GenericRequest>();
            }
            _requestQueue.Enqueue(request);
            CheckNextRequest();
        }

        private void CheckNextRequest()
        {
            if (_currentRequest != null) { return; }
            if (_requestQueue.Count == 0) { return; }

            _currentRequest = _requestQueue.Dequeue();
            ProcessRequest();
        }

        private void ProcessRequest()
        {
            CoroutineHelper.StartCorotine(WaitForRequest());
        }

        private IEnumerator WaitForRequest()
        {
            UnityWebRequest request = new UnityWebRequest();
            request.url = _currentRequest.URL;
            request.method = _currentRequest.Method;
            request.downloadHandler = new DownloadHandlerBuffer();
#if DEBUG_REQUEST
            Debug.Log ("=========================");
            Debug.Log ("<b>Request</b>");
            Debug.Log ("URL : " + request.url);
            Debug.Log ("Method : " + request.method);
            Debug.Log ("data : " + _currentRequest.GetRequestDataString ());
#endif

            request.SetRequestHeader("Content-type", "application/json");

            if (_currentRequest is RequestWithAuthentication)
            {
                foreach (var kvp in (_currentRequest as RequestWithAuthentication).GetAuthHeaders())
                {
                    request.SetRequestHeader(kvp.Key, kvp.Value);
                }
            }

            var payload = _currentRequest.GetRequestData();
            if (payload != null)
            {
                UploadHandlerRaw handlerRaw = new UploadHandlerRaw(payload);
                request.uploadHandler = handlerRaw;
            }

            yield return request.SendWebRequest();

#if DEBUG_REQUEST
            Debug.Log ("Response code : " + request.responseCode);
            Debug.Log ("Response msg : " + (string.IsNullOrEmpty (request.error) ? "Success" : request.error));
            Debug.Log ("Response msg : " + (string.IsNullOrEmpty (request.error) ? request.downloadHandler.text : request.downloadHandler.text));
            Debug.Log ("=========================");
#endif

            //_currentRequest.OnRequestDone(request);

            OnRequestIsDone(request);
        }

        private void OnRequestIsDone(UnityWebRequest ww)
        {
            if (_currentRequest is LoginRequest ||
                _currentRequest is LoginFacebookRequest ||
                _currentRequest is RegisterRequest ||
                _currentRequest is LoginGoogleRequest ||
                _currentRequest is TokenRenewerRequest)
            {
                if (ww.responseCode < 300)
                {
                    _tokenManager.SetLoggedInUser(ww.downloadHandler.text);
                    //Debug.Log("Login : " + (string.IsNullOrEmpty(ww.error) ? "Success" : $"Failed, {ww.responseCode.ToString()}"));

                }
                else
                {
                    Debug.Log("Auth token error");
                }
            }
            else if (_currentRequest is RegisterUserTokenRequst)
            {
                if (ww.responseCode < 300)
                {
                    try
                    {
                        TokenResponse response = JsonUtility.FromJson<TokenResponse>(ww.downloadHandler.text);
                        _tokenManager.SetUserToken(response.token);
                    }
                    catch (Exception e)
                    {
                        Debug.LogError("Wrong format RegisterUserTokenRequest data ");
                        Debug.LogError($"Error : {e}");
                    }
                }
                else
                {
                    Debug.Log($"Log : {ww.responseCode} {ww.error}");
                    Debug.LogError("RegisterUserTokenRequest failed");
                }
            }
            else if (_currentRequest is GetUserDetailsRequest)
            {
                if (ww.responseCode < 300)
                {
                    try
                    {
                        UserDetails data = JsonUtility.FromJson<UserDetails>(ww.downloadHandler.text);
                        var login_method = data.login_method;
                        if (login_method == "username" ||
                            login_method == "google" ||
                            login_method == "facebook")
                            _tokenManager.SetUserIsLoginByMethod(true);
                        else
                            _tokenManager.SetUserIsLoginByMethod(false);
                    }
                    catch (Exception e)
                    {
                        Debug.LogError("Cannot set verified user data");
                        Debug.LogError($"Error : {e}");
                        _tokenManager.SetUserIsLoginByMethod(false);
                    }
                }
            }
            else if (_currentRequest is LogoutRequest)
            {
                _tokenManager.ResetAuthData();
            }

            _currentRequest.OnRequestDone(ww);

            ww.Dispose();
            ww = null;
            _currentRequest = null;
            CheckNextRequest();
        }
    }

    /// <summary>
    /// Coroutine helper. Provides functionality to start corotines from non
    /// unity MonoBehaviour objects;
    /// </summary>
    internal class CoroutineHelper : MonoBehaviour
    {
        private static CoroutineHelper _instnace;

        private static void SpawnInstnace()
        {
            GameObject go = new GameObject();
            _instnace = go.AddComponent<CoroutineHelper>();
            go.name = "CoroutineHelper";
        }

        /// <summary>
        /// Create the instance of CorotineHelper if needed 
        /// and starts the given corotine.
        /// </summary>
        /// <param name="routine">Routine.</param>
        internal static Coroutine StartCorotine(IEnumerator routine)
        {
            if (_instnace == null)
            {
                SpawnInstnace();
            }
            return _instnace.StartCoroutine(routine);
        }

        /// <summary>
        /// Create the instance of CorotineHelper if needed and stops 
        /// the given corotine.
        /// </summary>
        /// <param name="coroutine">Coroutine.</param>
        internal static void StopCorotine(Coroutine coroutine)
        {
            if (_instnace == null)
            {
                SpawnInstnace();
            }
            _instnace.StopCoroutine(coroutine);
        }
    }

    internal class LocalDataProvider
    {
        /// <summary>
        /// Gets the installation identifier.
        /// </summary>
        internal static string GetInstallationId()
        {
            return "instl_" + DateTime.Now.Ticks;
        }
    }

    [Serializable]
    internal class GoogleAPIResponse
    {
        public string access_token;
        public int expires_in;
        public string refresh_token;
        public string scope;
        public string token_type;
        public string id_token;
    }

    public class TokenResponse
    {
        public string token;
    }
}