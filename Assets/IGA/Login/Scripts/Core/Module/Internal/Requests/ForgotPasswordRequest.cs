﻿using System.Text;
using UnityEngine;
using UnityEngine.Networking;


namespace HotPlay.Internal.Requests
{
    internal class ForgotPasswordRequest : RequestWithAuthentication
    {
        internal override void SetupRequest(RequestData data)
        {
            this.data = data;
            data.Method = "POST";
            data.URL = Constants.BaseURL + Constants.ForgotPasswordSubURL;
        }

        internal override byte[] GetRequestData()
        {
            var d = data as ForgotPassRequestData;
            string s = "{\"email\": \"" + d.email + "\" }";
            //Debug.Log(s);
            byte[] bytes = Encoding.UTF8.GetBytes(s);
            return bytes;
        }

        internal override string GetRequestDataString()
        {
            string s = JsonUtility.ToJson(data as ForgotPassRequestData);
            return s;
        }

        internal override void OnRequestDone(UnityWebRequest www)
        {
            var authd = data as ForgotPassRequestData;
            if (www.error != null)
            {
                authd.OnFailedDelegate?.Invoke(www.error);
                return;
            }
            string jwt = www.downloadHandler.text;
            authd.OnSuccessDelegate?.Invoke(jwt);
        }
    }

    internal class ForgotPassRequestData : RequestData
    {
        internal string email;
        internal StringCallback OnSuccessDelegate;
        internal StringCallback OnFailedDelegate;
    }
}