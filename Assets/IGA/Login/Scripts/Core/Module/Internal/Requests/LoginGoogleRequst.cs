﻿using System.Text;
using UnityEngine;
using UnityEngine.Networking;


namespace HotPlay.Internal.Requests
{
    internal class LoginGoogleRequest : RequestWithAuthentication
    {
        internal override void SetupRequest(RequestData data)
        {
            this.data = data;
            data.Method = "POST";
            data.URL = Constants.BaseURL + Constants.LoginGoogleSubURL;
        }

        internal override byte[] GetRequestData()
        {
            string s = JsonUtility.ToJson(data as GoogleAuthRequestData);
            byte[] bytes = Encoding.UTF8.GetBytes(s);
            return bytes;
        }

        internal override string GetRequestDataString()
        {
            string s = JsonUtility.ToJson(data as GoogleAuthRequestData);
            return s;
        }

        internal override void OnRequestDone(UnityWebRequest www)
        {
            var authd = data as GoogleAuthRequestData;
            if (www.error != null)
            {
                GameObject.Find("LoginModule(Clone)").SendMessage("LogoutGoogle", SendMessageOptions.RequireReceiver);
                authd.OnFailedDelegate?.Invoke(www.error);
                return;
            }
            string jwt = www.downloadHandler.text;
            authd.OnSuccessDelegate?.Invoke(jwt);
        }
    }

    [System.Serializable]
    internal class GoogleAuthRequestData : RequestData
    {
        [SerializeField]
        internal string access_token;

        internal StringCallback OnSuccessDelegate;
        internal StringCallback OnFailedDelegate;
    }
}