using System.Text;
using UnityEngine;
using UnityEngine.Networking;

namespace HotPlay.Internal.Requests
{
    internal class TokenRenewerRequest : RequestWithAuthentication
    {
        internal override void SetupRequest(RequestData data)
        {
            this.data = data;
            data.Method = "GET";
            data.URL = Constants.BaseURL + Constants.TokenRenewerURL;
        }

        internal override byte[] GetRequestData()
        {
            return null;
        }

        internal override string GetRequestDataString()
        {
            return "";
        }

        internal override void OnRequestDone(UnityWebRequest www)
        {
            var authd = data as TokenRenewerRequstData;
            if (www.error != null)
            {
                authd.OnFailedDelegate?.Invoke(www.error);
                return;
            }
            string message = www.downloadHandler.text;
            authd.OnSuccessDelegate?.Invoke(message);
        }
    }

    [System.Serializable]
    internal class TokenRenewerRequstData : RequestData
    {
        internal StringCallback OnSuccessDelegate;
        internal StringCallback OnFailedDelegate;
    }
}