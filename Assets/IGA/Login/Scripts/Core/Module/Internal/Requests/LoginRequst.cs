﻿using System.Text;
using UnityEngine;
using UnityEngine.Networking;

namespace HotPlay.Internal.Requests
{
    internal class LoginRequest : RequestWithAuthentication
    {
        internal override void SetupRequest(RequestData data)
        {
            this.data = data;
            data.Method = "POST";
            data.URL = Constants.BaseURL + Constants.LoginSubURL;
        }

        internal override byte[] GetRequestData()
        {
            string s = JsonUtility.ToJson(data as AuthRequestData);
            byte[] bytes = Encoding.UTF8.GetBytes(s);
            return bytes;
        }

        internal override string GetRequestDataString()
        {
            string s = JsonUtility.ToJson(data as AuthRequestData);
            return s;
        }

        internal override void OnRequestDone(UnityWebRequest www)
        {
            var authdata = data as AuthRequestData;
            if (www.error != null)
            {
                authdata.OnFailedDelegate?.Invoke(www.error);
                return;
            }
            string jwt = www.downloadHandler.text;
            authdata.OnSuccessDelegate?.Invoke(jwt);
        }
    }

    [System.Serializable]
    internal class AuthRequestData : RequestData
    {
        [SerializeField]
        internal string username;
        [SerializeField]
        internal string password;

        internal StringCallback OnSuccessDelegate;
        internal StringCallback OnFailedDelegate;
    }
}