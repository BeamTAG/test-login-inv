﻿using UnityEngine.Networking;
using UnityEngine;
using System.Text;

namespace HotPlay.Internal.Requests
{
    internal class RegisterUserTokenRequst : GenericRequest
    {
        internal override void SetupRequest(RequestData data)
        {
            this.data = data;
            data.Method = "POST";
            data.URL = Constants.RegisterUserTokenURL;
        }

        internal override byte[] GetRequestData()
        {
            string s = JsonUtility.ToJson(data as RegisterUserTokenRequstData);
            byte[] bytes = Encoding.UTF8.GetBytes(s);
            return bytes;
        }

        internal override string GetRequestDataString()
        {
            string s = JsonUtility.ToJson(data as RegisterUserTokenRequstData);
            return s;
        }

        internal override void OnRequestDone(UnityWebRequest www)
        {
            var authd = data as RegisterUserTokenRequstData;
            if (www.error != null)
            {
                authd.OnFailedDelegate?.Invoke(www.error);
                return;
            }
            string userToken = www.downloadHandler.text;
            authd.OnSuccessDelegate?.Invoke(userToken);
        }
    }

    [System.Serializable]
    internal class RegisterUserTokenRequstData : RequestData
    {
        [SerializeField]
        internal string installation_id;

        internal StringCallback OnSuccessDelegate;
        internal StringCallback OnFailedDelegate;
    }
}