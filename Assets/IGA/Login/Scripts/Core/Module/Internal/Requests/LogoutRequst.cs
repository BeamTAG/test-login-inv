﻿
using UnityEngine.Networking;

namespace HotPlay.Internal.Requests
{
    internal class LogoutRequest : RequestWithAuthentication
    {
        internal override void SetupRequest(RequestData data)
        {
            this.data = data;
            data.Method = "POST";
            data.URL = Constants.BaseURL + Constants.LogoutSubURL;
        }

        internal override byte[] GetRequestData()
        {
            return null;
        }

        internal override string GetRequestDataString()
        {
            return "";
        }

        internal override void OnRequestDone(UnityWebRequest www)
        {
            var reqData = data as LogoutRequestData;
            if (www.error != null)
            {
                reqData.OnFailedDelegate?.Invoke(www.error);
                return;
            }

            reqData.OnSuccessDelegate?.Invoke("success");
        }
    }

    internal class LogoutRequestData : RequestData
    {
        internal StringCallback OnSuccessDelegate;
        internal StringCallback OnFailedDelegate;
    }
}