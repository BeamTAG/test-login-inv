﻿using System.Text;
using UnityEngine;
using UnityEngine.Networking;


namespace HotPlay.Internal.Requests
{
    internal class ChangePasswordRequest : RequestWithAuthentication
    {
        internal override void SetupRequest(RequestData data)
        {
            this.data = data;
            data.Method = "POST";
            data.URL = Constants.BaseURL + Constants.ChangePasswordSubURL;
        }

        internal override byte[] GetRequestData()
        {
            var d = data as ChangePassRequestData;
            string s = "{\"old-password\": \"" + d.oldPass + "\" ,\"new-password\": \"" + d.newPass + "\" }";
            //Debug.Log(s);
            byte[] bytes = Encoding.UTF8.GetBytes(s);
            return bytes;
        }

        internal override string GetRequestDataString()
        {
            string s = JsonUtility.ToJson(data as ChangePassRequestData);
            return s;
        }

        internal override void OnRequestDone(UnityWebRequest www)
        {
            var authd = data as ChangePassRequestData;
            if (www.error != null)
            {
                if (authd.OnFailedDelegate != null) authd.OnFailedDelegate(www.error);
                return;
            }
            string jwt = www.downloadHandler.text;
            if (authd.OnSuccessDelegate != null) authd.OnSuccessDelegate(jwt);
        }
    }

    internal class ChangePassRequestData : RequestData
    {
        internal string oldPass;
        internal string newPass;
        internal StringCallback OnSuccessDelegate;
        internal StringCallback OnFailedDelegate;
    }
}