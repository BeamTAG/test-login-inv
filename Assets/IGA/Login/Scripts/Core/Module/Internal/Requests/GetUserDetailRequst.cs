﻿using UnityEngine.Networking;
using UnityEngine;

namespace HotPlay.Internal.Requests
{
    internal class GetUserDetailsRequest : RequestWithAuthentication
    {
        internal override void SetupRequest(RequestData data)
        {
            this.data = data;
            data.Method = "GET";
            data.URL = Constants.BaseURL + Constants.GetUserDetailsSubURL;
        }

        internal override byte[] GetRequestData()
        {
            return null;
        }

        internal override string GetRequestDataString()
        {
            return "";
        }

        internal override void OnRequestDone(UnityWebRequest www)
        {
            var authd = data as UserDetailsRequestData;
            if (www.error != null)
            {
                authd.OnFailedDelegate?.Invoke(www.error);
                return;
            }
            string jwt = www.downloadHandler.text;
            UserDetails details = JsonUtility.FromJson<UserDetails>(jwt);
            authd.OnSuccessDelegate?.Invoke(details);
        }
    }

    [System.Serializable]
    internal class UserDetailsRequestData : RequestData
    {
        internal ObjectCallback OnSuccessDelegate;
        internal StringCallback OnFailedDelegate;
    }

    [System.Serializable]
    public class UserDetails
    {
        public string email;
        public string expires;
        public string full_name;
        public string id;
        public string installation_id;
        public string login_method;
        public string referral_code;
        public string referred_by;
        public string referred_link;
    }
}