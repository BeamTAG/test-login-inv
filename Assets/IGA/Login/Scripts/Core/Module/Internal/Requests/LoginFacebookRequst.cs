﻿using System.Text;
using UnityEngine;
using UnityEngine.Networking;


namespace HotPlay.Internal.Requests
{
    internal class LoginFacebookRequest : RequestWithAuthentication
    {
        internal override void SetupRequest(RequestData data)
        {
            this.data = data;
            data.Method = "POST";
            data.URL = Constants.BaseURL + Constants.LoginFacebookSubURL;
        }

        internal override byte[] GetRequestData()
        {
            string s = JsonUtility.ToJson(data as FacebookAuthRequestData);
            byte[] bytes = Encoding.UTF8.GetBytes(s);
            return bytes;
        }

        internal override string GetRequestDataString()
        {
            string s = JsonUtility.ToJson(data as FacebookAuthRequestData);
            return s;
        }

        internal override void OnRequestDone(UnityWebRequest www)
        {
            var authData = data as FacebookAuthRequestData;
            if (www.error != null)
            {
                authData.OnFailedDelegate?.Invoke(www.error);
                //Facebook.Unity.FB.LogOut();
                return;
            }
            string jwt = www.downloadHandler.text;
            authData.OnSuccessDelegate?.Invoke(jwt);
        }
    }

    [System.Serializable]
    internal class FacebookAuthRequestData : RequestData
    {
        [SerializeField]
        internal string access_token;

        internal StringCallback OnSuccessDelegate;
        internal StringCallback OnFailedDelegate;
    }
}