﻿using UnityEngine;
using System.Text;
using UnityEngine.Networking;

namespace HotPlay.Internal.Requests
{
    internal class RegisterRequest : RequestWithAuthentication
    {
        internal override void SetupRequest(RequestData data)
        {
            this.data = data;
            data.Method = "POST";
            data.URL = Constants.BaseURL + Constants.RegisterSubURL;
        }

        internal override byte[] GetRequestData()
        {
            string s = JsonUtility.ToJson(data as RegisterRequestData);
            byte[] bytes = Encoding.UTF8.GetBytes(s);
            return bytes;
        }

        internal override string GetRequestDataString()
        {
            string s = JsonUtility.ToJson(data as RegisterRequestData);
            return s;
        }

        internal override void OnRequestDone(UnityWebRequest www)
        {
            var authd = data as RegisterRequestData;

            if (www.error != null)
            {
                authd.OnFailedDelegate?.Invoke(www.error);
                return;
            }
            string jwt = www.downloadHandler.text;
            authd.OnSuccessDelegate?.Invoke(jwt);
        }
    }

    [System.Serializable]
    internal class RegisterRequestData : RequestData
    {
        [SerializeField]
        internal string username;
        [SerializeField]
        internal string password;
        [SerializeField]
        internal string full_name;
        [SerializeField]
        internal string email;

        internal StringCallback OnSuccessDelegate;
        internal StringCallback OnFailedDelegate;
    }
}