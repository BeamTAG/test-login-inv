﻿using System.Collections.Generic;
//using Facebook.Unity;
using Google;
using HotPlay.Internal;
using HotPlay.Internal.Requests;
using HotPlay.Internal.Token;
using UnityEngine;
using UnityEngine.SceneManagement;
using Constants = HotPlay.Internal.Constants;

namespace HotPlay
{
    internal class IGA_LoginInterface
    {
        internal static string LastUsedLoginMethod;

        private static RequestQueue _queue;
        private static FacebookAuthRequestData _fbLoginData;
        private static GoogleAuthRequestData _googleLoginData;

        private static TokenManager _tokenManager = new TokenManager();

        public static string AccessToken
        {
            get
            {
                return _tokenManager.GetUserToken();
            }
        }

        public static void InitModule(string registerInstallationURL,
            BoolCallback onInitializationDone,
            string selectedLang,
            string baseBackendURL)
        {
            if (!string.IsNullOrEmpty(baseBackendURL))
            {
                Constants.BaseURL = baseBackendURL + "/auth";
            }

            if (_tokenManager.IsLoggedInBefore())
            {
                //Auto renew auth token
                TokenRenew((message) =>
                {
                    localization.Localization.Instance.Initialize(baseBackendURL, selectedLang, (m, b) =>
                    {
                        onInitializationDone(b);
                    });
                }, (message) =>
                {
                    onInitializationDone(false);
                });
            }
            else
            {
                RegisterUserToken(LocalDataProvider.GetInstallationId(), registerInstallationURL,
                   successDelegate: (message) =>
                   {
                       localization.Localization.Instance.Initialize(baseBackendURL, selectedLang, (m, b) =>
                       {
                           onInitializationDone(b);
                       });
                   },
                   failedDelegate: (message) =>
                   {
                       onInitializationDone(false);
                   });
            }
        }

        /// <summary>
        /// Initialize Login actions.
        /// </summary>
        public static void Login(string login, string pass,
            StringCallback successDelegate,
            StringCallback failedDelegate)
        {
            LastUsedLoginMethod = "hotnow";
            AuthRequestData data = new AuthRequestData
            {
                username = login,
                password = pass,
                OnSuccessDelegate = successDelegate,
                OnFailedDelegate = failedDelegate,
                RequstType = RequstType.Login
            };
            GenericRequest request = RequestFactory.CreateRequest(data);
            PutToQueue(request);
        }

        internal static void LoginFacebook(StringCallback successDelegate, StringCallback failedDelegate)
        {
            LastUsedLoginMethod = "facebook";
            _fbLoginData = new FacebookAuthRequestData
            {
                OnSuccessDelegate = successDelegate,
                OnFailedDelegate = failedDelegate,
                RequstType = RequstType.LoginFacebook
            };
            InitFacebookLoginFlow();
        }

        internal static void LoginGoogle(StringCallback successDelegate, StringCallback failedDelegate)
        {
            LastUsedLoginMethod = "google";

            _googleLoginData = new GoogleAuthRequestData
            {
                OnSuccessDelegate = successDelegate,
                OnFailedDelegate = failedDelegate,
                RequstType = RequstType.LoginGoogle
            };

            InitGoogleLoginFlow();
        }

        private static void InitFacebookLoginFlow()
        {
            //if (!FB.IsInitialized)
            //{
            //    FB.Init(onInitComplete: OnFBInited);
            //    return;
            //}
            //if (!FB.IsLoggedIn)
            //{
            //    FB.LogInWithReadPermissions(new List<string>() { "email", "user_friends" }, OnFBLoggedIn);
            //    return;
            //}

            //_fbLoginData.access_token = Facebook.Unity.AccessToken.CurrentAccessToken.TokenString;
            //GenericRequest request = RequestFactory.CreateRequest(_fbLoginData);
            //_fbLoginData = null;
            //PutToQueue(request);
        }

        //private static void OnFBLoggedIn(ILoginResult result)
        //{
        //    if (result.Cancelled || !string.IsNullOrEmpty(result.Error))
        //    {
        //        _fbLoginData.OnFailedDelegate(ErrorType.LoginErrorByCancel.ToString());
        //        _fbLoginData = null;
        //        return;
        //    }
        //    InitFacebookLoginFlow();
        //}

        private static void OnFBInited()
        {
            InitFacebookLoginFlow();
        }

        private static void InitGoogleLoginFlow()
        {
            GoogleRequestCallbacks cb = new GoogleRequestCallbacks()
            {
                successDelegate = (token) =>
                {
                    _googleLoginData.access_token = token;
                    GenericRequest request = RequestFactory.CreateRequest(_googleLoginData);
                    _googleLoginData = null;
                    PutToQueue(request);
                },
                failedDelegate = (message) =>
                {
                    _googleLoginData.OnFailedDelegate(ErrorType.LoginErrorByCancel.ToString());
                    _googleLoginData = null;
                    return;
                }
            };

            GameObject.Find("LoginModule(Clone)").SendMessage("LoginGoogle", cb, SendMessageOptions.RequireReceiver);
        }

        /// <summary>
        /// initialize Registration with specified username, password, fullname, email.
        /// will call successDelegate in case of successfull registration OR 
        /// failedDelegate in case of any fail.
        /// </summary>
        /// <param name="username">Username.</param>
        /// <param name="password">Password.</param>
        /// <param name="fullname">Fullname.</param>
        /// <param name="email">Email.</param>
        /// <param name="successDelegate">Success delegate.</param>
        /// <param name="failedDelegate">Failed delegate.</param>
        public static void Register(string username,
            string password,
            string fullname,
            string email,
            StringCallback successDelegate,
            StringCallback failedDelegate)
        {
            RegisterRequestData data = new RegisterRequestData()
            {
                email = email,
                username = username,
                full_name = fullname,
                password = password,
                RequstType = RequstType.Register,
                OnFailedDelegate = failedDelegate,
                OnSuccessDelegate = successDelegate,
            };
            GenericRequest request = RequestFactory.CreateRequest(data);
            PutToQueue(request);
        }

        public static void Logout(StringCallback successDelegate,
            StringCallback failedDelegate)
        {
            if (LastUsedLoginMethod == "google")
            {
                GoogleSignIn.DefaultInstance.SignOut();
                GoogleSignIn.Configuration = null;
                GoogleSignIn.DefaultInstance.DestroyInstance();
            }
            else if (LastUsedLoginMethod == "facebook")
            {
                //FB.LogOut();
            }

            LogoutRequestData data = new LogoutRequestData()
            {
                RequstType = RequstType.Logout,
                OnFailedDelegate = failedDelegate,
                OnSuccessDelegate = successDelegate
            };
            GenericRequest request = RequestFactory.CreateRequest(data);
            PutToQueue(request);
        }

        public static void GetUserData(ObjectCallback successDelegate,
            StringCallback failedDelegate)
        {
            UserDetailsRequestData data = new UserDetailsRequestData()
            {
                RequstType = RequstType.GetUserDetail,
                OnFailedDelegate = failedDelegate,
                OnSuccessDelegate = successDelegate
            };
            GenericRequest request = RequestFactory.CreateRequest(data);
            PutToQueue(request);
        }

        public static void ChangePassword(string oldPass,
            string newPass,
            StringCallback successDelegate,
            StringCallback failedDelegate)
        {
            ChangePassRequestData data = new ChangePassRequestData()
            {
                newPass = newPass,
                oldPass = oldPass,
                RequstType = RequstType.ChangePassword,
                OnSuccessDelegate = successDelegate,
                OnFailedDelegate = failedDelegate
            };
            GenericRequest request = RequestFactory.CreateRequest(data);
            PutToQueue(request);
        }

        public static void ForgotPasswordPassword(string email,
            StringCallback successDelegate,
            StringCallback failedDelegate)
        {
            ForgotPassRequestData data = new ForgotPassRequestData()
            {
                email = email,
                RequstType = RequstType.ForgotPassword,
                OnSuccessDelegate = successDelegate,
                OnFailedDelegate = failedDelegate
            };
            GenericRequest request = RequestFactory.CreateRequest(data);
            PutToQueue(request);
        }

        /// <summary>
        /// Register user token
        /// </summary>
        /// <param name="installation_id">Installation ID.</param>
        /// <param name="successDelegate">Success delegate.</param>
        /// <param name="failedDelegate">Failed delegate.</param>
        public static void RegisterUserToken(string installation_id,
            string url,
            StringCallback successDelegate,
            StringCallback failedDelegate)
        {
            RegisterUserTokenRequstData data = new RegisterUserTokenRequstData()
            {
                installation_id = installation_id,
                RequstType = RequstType.RegisterUserToken,
                OnFailedDelegate = failedDelegate,
                OnSuccessDelegate = successDelegate,
            };
            Constants.RegisterUserTokenURL = url;
            GenericRequest request = RequestFactory.CreateRequest(data);
            PutToQueue(request);
        }

        public static void TokenRenew(StringCallback successDelegate, StringCallback failedDelegate)
        {
            TokenRenewerRequstData data = new TokenRenewerRequstData()
            {
                RequstType = RequstType.TokenRenewer,
                OnFailedDelegate = failedDelegate,
                OnSuccessDelegate = successDelegate
            };
            GenericRequest request = RequestFactory.CreateRequest(data);
            PutToQueue(request);
        }

        public static bool IsLoggedIn()
        {
            //return LocalDataProvider.IsLoggedIn();
            return _tokenManager.IsLoggedInBefore();
        }

        public static bool IsGuest()
        {
            return _tokenManager.IsGuestUser();
        }

        private static void PutToQueue(GenericRequest request)
        {
            if (_queue == null) { _queue = new RequestQueue(); }
            _queue.AddRequest(request);
        }
    }

    public class GoogleRequestCallbacks
    {
        public StringCallback successDelegate;
        public StringCallback failedDelegate;
    }

    #region delegates 

    /// <summary>
    /// delegate that provide true/false status about was the 
    /// action resolved successfully and the string message.
    /// </summary>
    public delegate void ActionResultCallback(bool isActionSuccess, string message);
    public delegate void StringCallback(string message);
    public delegate void ObjectCallback(object obj);
    public delegate void SimpleCallback();
    public delegate void BoolCallback(bool result);
    #endregion
}