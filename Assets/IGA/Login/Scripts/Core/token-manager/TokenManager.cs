﻿using System;
using UnityEngine;

namespace HotPlay.Internal.Token
{
    public class TokenManager
    {
        private string _userTokenDataKey = "9060ce0e220581403564167a180058ef";
        private string _isLoggedInBeforeKey = "8ad74e9fa8f7c0c4faaf807309c92228";
        private string _isLoggedinByMethodKey = "4b5f9b721945cdb037e0ce79cbc1acc6";

        public TokenManager()
        {

        }

        public TokenManager(string userTokenDataKey, string isLoggedInBeforeKey, string isLoggedInByMethod)
        {
            _userTokenDataKey = userTokenDataKey;
            _isLoggedInBeforeKey = isLoggedInBeforeKey;
            _isLoggedinByMethodKey = isLoggedInByMethod;
        }

        /// <summary>
        /// Save user token
        /// </summary>
        /// <param name="token"></param>
        public void SetUserToken(string token)
        {
            if (string.IsNullOrEmpty(token))
            {
                throw new ArgumentNullException($"Please input token before setUserToken");
            }
            else
            {
                PlayerPrefs.SetString(_userTokenDataKey, token);
            }
        }

        /// <summary>
        /// Get user token
        /// </summary>
        /// <returns></returns>
        public string GetUserToken()
        {
            return PlayerPrefs.GetString(_userTokenDataKey);
        }

        /// <summary>
        /// Return true if logged in before by user or guest
        /// </summary>
        /// <returns></returns>
        public bool IsHasUserToken()
        {
            var userLoggedIn = PlayerPrefs.GetString(_userTokenDataKey, "");
            if (!String.IsNullOrEmpty(userLoggedIn))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Set user logged in
        /// </summary>
        /// <param name="token"></param>
        public void SetLoggedInUser(string token)
        {
            if (string.IsNullOrEmpty(token))
            {
                throw new ArgumentNullException($"Please input token before setLoggedInUser");
                ResetAuthData();
            }
            else
            {
                SetUserToken(token);
                PlayerPrefs.SetInt(_isLoggedInBeforeKey, 1);
            }
        }

        /// <summary>
        /// Get user logged in before
        /// </summary>
        /// <returns></returns>
        public bool IsLoggedInBefore()
        {
            var isLoggedInBefore = PlayerPrefs.GetInt(_isLoggedInBeforeKey, 0);
            if (isLoggedInBefore == 1)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Set user login by hotnow, google or facebook account (Set false if it guest account)
        /// </summary>
        /// <param name="isLoggedInByMethod"></param>
        public void SetUserIsLoginByMethod(bool isLoggedInByMethod)
        {
            if (isLoggedInByMethod)
            {
                PlayerPrefs.SetInt(_isLoggedinByMethodKey, 1);
            }
            else
            {
                PlayerPrefs.DeleteKey(_isLoggedinByMethodKey);
            }
        }

        /// <summary>
        /// Return true if user is guest
        /// </summary>
        /// <returns></returns>
        public bool IsGuestUser()
        {
            var isGuest = PlayerPrefs.GetInt(_isLoggedinByMethodKey, 0);
            return (isGuest == 1) ? false : true;
        }

        /// <summary>
        /// Reset auth data
        /// </summary>
        public void ResetAuthData()
        {
            PlayerPrefs.DeleteKey(_userTokenDataKey);
            PlayerPrefs.DeleteKey(_isLoggedInBeforeKey);
            PlayerPrefs.DeleteKey(_isLoggedinByMethodKey);
        }
    }
}
