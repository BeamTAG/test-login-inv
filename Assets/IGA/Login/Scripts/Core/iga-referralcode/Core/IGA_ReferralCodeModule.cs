﻿using System;
using UnityEngine;

namespace HotPlay.Internal.Referral
{
    public class IGA_ReferralCodeModule : MonoBehaviour
    {
        [SerializeField] private ReferralCodeAspect _referralAspect;

        private Action<bool, string> _onInputCodeSuccess;
        private SimpleCallback _onInputCodeFailed;

        public void ShowReferralCodePanel(Action<bool, string> successCallback, SimpleCallback failedCallback)
        {
            ShowPanel();
            _onInputCodeSuccess = successCallback;
            _onInputCodeFailed = failedCallback;
        }

        private void ShowPanel()
        {
            gameObject.SetActive(true);
        }

        private void HidePanel()
        {
            gameObject.SetActive(false);
        }

        private void Awake()
        {
            OnAddEventAction();
        }

        private void OnAddEventAction()
        {
            _referralAspect.EvReferralCodeProcess += OnActionSubmit;
            _referralAspect.EvReferralCodeSkip += OnActionSkip;
            _referralAspect.EvReferralSuccess += OnSuccessInputReferralCode;
        }

        private void OnActionSkip()
        {
            HidePanel();
            _onInputCodeSuccess?.Invoke(true, null);
        }

        private async void OnActionSubmit()
        {
            if (!isValidField())
            {
                _referralAspect.OnShowRequestError(ReferralErrorCode.InvalidInputField);
            }
            else if (!isValidLoginData())
            {
                _referralAspect.OnShowRequestError(ReferralErrorCode.InvalidLoginData);
            }
            else
            {
                var url = GetReferredByUrl();
                var headerToken = LoginManager.Instance.AccessToken;
                var payload = CreatePayload();

                IReferralCodeWebRequest request = new ReferralCodeWebRequest();
                var data = await request.OnRequestReferralCode(url, headerToken, payload);

                if (data.code == 200)
                {
                    _referralAspect.OnSubmitSuccess();
                }
                else
                {
                    _referralAspect.OnSubmitFailed();
                    if (data.code == 422)
                    {
                        _referralAspect.OnShowRequestError(ReferralErrorCode.InvalidRefferredBy);
                    }
                    else
                    {
                        _onInputCodeFailed?.Invoke();
                    }
                }
            }
        }

        private bool isValidField()
        {
            if (string.IsNullOrEmpty(_referralAspect._inputReferralCode.text))
            {
                return false;
            }

            return true;
        }

        private bool isValidLoginData()
        {
            if (string.IsNullOrEmpty(LoginManager.Instance.AccessToken) ||
                string.IsNullOrEmpty(LoginManager.Instance.ReferralLink))
            {
                return false;
            }
            return true;
        }

        private string CreatePayload()
        {
            var data = new ReferralRequestData();
            data.referred_by = _referralAspect._inputReferralCode.text.ToUpper();

            return JsonUtility.ToJson(data);
        }

        private string GetReferredByUrl()
        {
            var baseUrl = Constants.BaseURL;
            var referredUrl = LoginManager.Instance.ReferralLink;

            try
            {
                var path = baseUrl + referredUrl;
                return path;
            }
            catch (Exception e)
            {
                Debug.LogError($"Error : {e}");
                return String.Empty;
            }
        }

        private void OnSuccessInputReferralCode()
        {
            HidePanel();
            _onInputCodeSuccess?.Invoke(false, _referralAspect._inputReferralCode.text.ToUpper());
        }
    }
}