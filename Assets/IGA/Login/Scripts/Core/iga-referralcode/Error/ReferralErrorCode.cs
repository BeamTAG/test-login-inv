﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotPlay.Internal.Referral
{
    public class ReferralErrorCode
    {
        public static string InvalidRefferredBy => "invalid_referredby";
        public static string InvalidInputField => "invalid_inputfield";
        public static string InvalidLoginData => "invalid_login_data";
    }
}
