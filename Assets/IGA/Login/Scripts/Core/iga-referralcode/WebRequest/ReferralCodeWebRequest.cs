﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HotPlay;
using UnityEngine;
using UnityEngine.Networking;

namespace HotPlay.Internal.Referral
{
    class ReferralCodeWebRequest : IReferralCodeWebRequest
    {
        public async Task<CodeAndMsg> OnRequestReferralCode(string url, string headerAuthorization, string payload)
        {
            UnityWebRequest www = UnityWebRequest.Put(url, payload);
            www.SetRequestHeader("authorization", $"Bearer {headerAuthorization}");

            await www.SendWebRequest();

            CodeAndMsg codeAndMessage = new CodeAndMsg();
            if (www.isHttpError || www.isNetworkError)
            {
                codeAndMessage.code = www.responseCode;
                codeAndMessage.message = www.error;
            }
            else
            {
                codeAndMessage.code = www.responseCode;
                codeAndMessage.message = www.downloadHandler.text;
            }
            return codeAndMessage;
        }
    }
}
