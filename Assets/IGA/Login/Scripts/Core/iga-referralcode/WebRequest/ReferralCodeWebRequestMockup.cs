﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotPlay.Internal.Referral
{
    class ReferralCodeWebRequestMockup : IReferralCodeWebRequest
    {
        public async Task<CodeAndMsg> OnRequestReferralCode(string url, string headerAuthorization, string payload)
        {
            var task = await Task<CodeAndMsg>.Factory.StartNew(() =>
            {
                var data = new CodeAndMsg();
                data.code = 200;
                data.message = "{\"email\":\"neymar.jr@gmail.com\",\"expires\":null,\"full_name\":\"NeymarJr.\",\"id\":\"1549541470-IMLD_10c0fNrr4nfYNpG5xCxiBxtnp4S\",\"installation_id\":null,\"verified\":false\"login_method\":\"username\",\"referral_code\":\"1234ASDF5678\",\"referred_by\":\"ASDF1234ASDF\"\"referred_link\":\"/odin/app/api/app-user/invictus/1549541470-IMLD_10c0fNrr4nfYNpG5xCxiBxtnp4S/referred-by\"}";

                return data;
            });
            return task;
        }
    }
}
