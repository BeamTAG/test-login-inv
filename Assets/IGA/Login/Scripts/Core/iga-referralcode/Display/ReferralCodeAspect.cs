﻿using HotPlay.localization;
using HotPlay.UI;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace HotPlay.Internal.Referral
{
    public class ReferralCodeAspect : MonoBehaviour
    {
        public event Action EvReferralCodeProcess;
        public event Action EvReferralCodeSkip;
        public event Action EvReferralSuccess;

        public InputField _inputReferralCode;
        public Text _errorMessage;
        public Button _submitButton;
        public Button _skipButton;

        [SerializeField] LoadingFade _loadingFade;

        public void OnShowRequestError(string error)
        {
            ShowErrorMessage(error);
        }

        private void Awake()
        {
            OnAddButtonListener();

            _loadingFade.EvSuccessShown += EvReferralSuccess;
        }

        private void OnAddButtonListener()
        {
            AddButtonListener(_submitButton, OnClickSubmit);
            AddButtonListener(_skipButton, OnClickSkip);
        }

        private void OnClickSubmit()
        {
            if (Application.internetReachability == NetworkReachability.NotReachable)
            {
                ShowErrorMessage("Cannot resolve destination host");
                return;
            }

            OnWaitingProcess();
            EvReferralCodeProcess?.Invoke();
        }

        private void OnClickSkip()
        {
            EvReferralCodeSkip?.Invoke();
        }

        private void AddButtonListener(Button button, Action action)
        {
            button.onClick.AddListener(() =>
            {
                action?.Invoke();
            });
        }

        private void ShowErrorMessage(string message)
        {
            _loadingFade.HideLoadingFade();
            Debug.Log("ShowErrorMessage  " + message);
            if (string.IsNullOrEmpty(message))
            {
                _errorMessage.gameObject.SetActive(false);
                _errorMessage.SetAllDirty();
                return;
            }
            else if (message == "Cannot resolve destination host")
            {
                _errorMessage.text = Localization.Instance.Get("error_nointernet");
            }
            else if (message == ReferralErrorCode.InvalidRefferredBy)
            {
                _errorMessage.text = Localization.Instance.Get("error_invalid_referred_by");
            }
            else if (message == ReferralErrorCode.InvalidInputField)
            {
                _errorMessage.text = Localization.Instance.Get("error_invalid_referred_inputfield");
            }
            else if (message == ReferralErrorCode.InvalidLoginData)
            {
                _errorMessage.text = Localization.Instance.Get("error_invalid_login_data");
            }
            _errorMessage.gameObject.SetActive(true);
            _errorMessage.SetAllDirty();
        }

        private void HideErrorMessage()
        {
            _errorMessage.gameObject.SetActive(false);
        }

        public void OnWaitingProcess()
        {
            _loadingFade.ShowWait();
        }

        public void OnSubmitSuccess()
        {
            _loadingFade.ShowSuccess(1.5f);
            HideErrorMessage();
        }

        public void OnSubmitFailed()
        {
            _loadingFade.HideLoadingFade();
        }
    }
}
