﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace HotPlay.localization
{
    /// <summary>
    /// class used to localize Unity text labels.
    /// </summary>
    [RequireComponent(typeof(Text))]
    public class LocalizeUnityText : MonoBehaviour
    {
        public string LocalizationKey;

        private Text _textMesh;
        private bool _localized = false;

        private void Start()
        {
            Localization.EvLanguageChanged += OnLocalizationChanged;
            SetupLabel();
        }

        public void OnLocalizationChanged()
        {
            _localized = false;
            SetupLabel();
        }

        public void OnEnable()
        {
            if (!_localized)
            {
                SetupLabel();
            }
        }

        private void SetupLabel()
        {
            if (gameObject == null)
            {
                Localization.EvLanguageChanged -= OnLocalizationChanged;
                return;
            }

            if (_textMesh == null)
            {
                _textMesh = GetComponent<Text>();
            }
            if (_textMesh == null)
            {
                return;
            }
            _textMesh.text = Localization.Instance.Get(LocalizationKey);
            _localized = true;
        }

        public void OnDestroy()
        {
            Localization.EvLanguageChanged -= OnLocalizationChanged;
        }

    }
}