﻿using System;
using System.Collections.Generic;
using UnityEngine;
using HotPlay.Internal;
using System.Collections;
using UnityEngine.Networking;

namespace HotPlay.localization
{
    public class Localization
    {
        public static event Action EvLanguageChanged;

        public static Localization Instance
        {
            get
            {
                if (_instance == null) { _instance = new Localization(); }
                return _instance;
            }
        }

        private static Localization _instance;

        public string SelectedLangKey;

        private Dictionary<string, Dictionary<string, object>> _config;
        private Dictionary<string, object> _selectedLang;
        private Action<string, bool> _oninitializationDone;

        private Localization()
        {
            SelectedLangKey = "NaN";
            _config = null;
            _selectedLang = null;
            ReadDefaultLocalization();
        }

        internal void Initialize(string baseBackendURL, string selectedlang, Action<string, bool> oninitializationDone)
        {
            _oninitializationDone = oninitializationDone;
            CoroutineHelper.StartCorotine(GetLocalization(baseBackendURL, selectedlang));
        }

        public string Get(string key)
        {
            if (_selectedLang == null) { return key; }
            if (_selectedLang.ContainsKey(key))
            {
                return _selectedLang[key] as string;
            }
            return key;
        }

        public void SetLanguage(string langKey)
        {
            if (SelectedLangKey != langKey)
            {
                if (_config.ContainsKey(langKey))
                {
                    _selectedLang = _config[langKey];
                    SelectedLangKey = langKey;

                    EvLanguageChanged?.Invoke();
                }
            }
        }

        private void ReadDefaultLocalization()
        {
            var ta = Resources.Load<TextAsset>("Localization/defaultLocalization");
            if (ta != null)
            {
                ParseLocalizationJson(ta.text);
                ta = null;
                SetLanguage("en");
            }
        }

        private void ParseLocalizationJson(string data)
        {
            //var s = (Dictionary<string, object>)Facebook.MiniJSON.Json.Deserialize(data);

            _config = new Dictionary<string, Dictionary<string, object>>();
            //foreach (var key in s.Keys)
            //{
            //    Dictionary<string, object> dictionary = ((Dictionary<string, object>)s[key]);
            //    _config.Add(key, dictionary);
            //}
        }

        private void SetLocalizationData(Dictionary<string, Dictionary<string, object>> cfg)
        {
            _config = cfg;
            SelectedLangKey = "eng";
            _selectedLang = _config[SelectedLangKey];
        }

        private IEnumerator GetLocalization(string baseBackendURL, string selectedlang)
        {
            UnityWebRequest urlDefineRequest = new UnityWebRequest();
            string beBase = baseBackendURL.Replace(@"https://", @"https://auth.");
            string be = beBase + "/translations/index.json";

            urlDefineRequest.url = be;
            urlDefineRequest.downloadHandler = new DownloadHandlerBuffer();
            yield return urlDefineRequest.Send();

            if (!String.IsNullOrEmpty(urlDefineRequest.error))
            {
                Debug.Log("Login Localization: use default because of the error:" + urlDefineRequest.error);
                ReadDefaultLocalization();
                _oninitializationDone("locale", false);
                yield break;
            }

            var jsonstring = urlDefineRequest.downloadHandler.text;
            urlDefineRequest.Dispose();
            urlDefineRequest = null;

            var jsonData = new Dictionary<string, object>();//(Dictionary<string, object>)Facebook.MiniJSON.Json.Deserialize(jsonstring);

            if (!jsonData.ContainsKey("login"))
            {
                Debug.Log("Login Localization: use default because of path define request does not contains localizations");
                ReadDefaultLocalization();
                _oninitializationDone("locale", false);
                yield break;
            }

            string path = jsonData["login"].ToString();
            path = path.TrimStart('.');
            string beLoc = beBase + "/translations" + path;

            UnityWebRequest localeRequest = new UnityWebRequest();
            localeRequest.url = beLoc;
            localeRequest.downloadHandler = new DownloadHandlerBuffer();
            yield return localeRequest.Send();

            if (!String.IsNullOrEmpty(localeRequest.error))
            {
                Debug.Log("Login Localization: use default because of an error: " + localeRequest.error);
                ReadDefaultLocalization();
                _oninitializationDone("locale", false);
                yield break;
            }

            jsonstring = localeRequest.downloadHandler.text;
            localeRequest.Dispose();
            localeRequest = null;
            ParseLocalizationJson(jsonstring);
            SetLanguage(selectedlang);
            _oninitializationDone("locale", true);
        }
    }
}
