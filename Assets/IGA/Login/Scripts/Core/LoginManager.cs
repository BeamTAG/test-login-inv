﻿using System;
using System.Text.RegularExpressions;
using HotPlay.Internal.Referral;
using HotPlay.Internal.Requests;
using HotPlay.Internal.Token;
using UnityEngine;
using Localization = HotPlay.localization.Localization;

namespace HotPlay
{
    public class LoginManager
    {
        /// <summary>
        /// returns the instance of LoginManager.
        /// </summary>
        public static LoginManager Instance
        {
            get
            {
                if (_instance == null) { _instance = new LoginManager(); }
                return _instance;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this user is logged in.
        /// </summary>
        public bool IsLoggedIn { get { return IGA_LoginInterface.IsLoggedIn(); } }

        /// <summary>
        /// Gets a value that tell that user are guest or not
        /// </summary>
        public bool IsGuest { get { return IGA_LoginInterface.IsGuest(); } }

        /// <summary>
        /// Gets a value that
        /// </summary>
        public bool HasRedeemedCode
        {
            get
            {
                if (string.IsNullOrEmpty(UserDetails.referred_by))
                    return false;
                return true;
            }
        }

        /// <summary>
        /// Gets the user details.
        /// </summary>
        /// <value>The user details.</value>
        public UserDetails UserDetails { get; private set; }

        /// <summary>
        /// Gets or sets the access token.
        /// </summary>
        /// <value>The access token.</value>
        public string AccessToken { get { return IGA_LoginInterface.AccessToken; } }

        public string ReferralCode { get { return UserDetails.referral_code; } }
        public string ReferralLink { get { return UserDetails.referred_link; } }

        /// <summary>
        /// Gets the current localization key
        /// </summary>
        public string CurrentLocalization { get { return Localization.Instance.SelectedLangKey; } }

        private TokenManager tokenManager = new TokenManager();

        /// <summary>
        /// Initializes this instance of Login module
        /// </summary>
        public void InitManager(
            string registerInstallURL,
            string baseBackendURL,
            string selectedLang,
            BoolCallback OnLogininited)
        {
            IGA_LoginInterface.InitModule(registerInstallURL, OnLogininited, selectedLang, baseBackendURL);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="onLoginSuccess"></param>
        /// <param name="onLoginFailed"></param>
        public void ShowLogin(SimpleCallback onLoginSuccess, SimpleCallback onLoginFailed, bool isStartingGame = false)
        {
            if (isStartingGame)
            {
                if (tokenManager.IsLoggedInBefore() == false)
                {
                    Debug.Log("Not login");
                    ShowLoginPanel(onLoginSuccess, onLoginFailed);
                }
                else
                {
                    Debug.Log("Logged in");
                    GetUserDetails(onLoginSuccess, onLoginFailed);
                }
            }
            else
            {
                if (tokenManager.IsGuestUser() == true)
                {
                    Debug.Log("Guess login");
                    ShowLoginPanel(onLoginSuccess, onLoginFailed);
                }
                else
                {
                    Debug.LogError("User is logged in by some method please logout before");
                    onLoginFailed?.Invoke();

                }
            }
        }

        private void ShowLoginPanel(SimpleCallback onLoginSuccess, SimpleCallback onLoginFailed)
        {
            CreateLoginModuleObject();

            loginModuleComponent.ShowLogin(
                //Success
                () =>
                {
                    GetUserDetails(onLoginSuccess, onLoginFailed);
                },
                //Failed
                () => { onLoginFailed(); }
            );
        }

        /// <summary>
        /// Logout the logged-in user
        /// </summary>
        public void Logout(SimpleCallback OnLoggedOut)
        {
            IGA_LoginInterface.Logout(
                (val) => { OnLoggedOut(); },
                (val) => { OnLoggedOut(); }
            );
        }

        /// <summary>
        /// User login by username and password
        /// </summary>
        /// <param name="username">username</param>
        /// <param name="password">password</param>
        /// <param name="onSuccessCallback">success delegate</param>
        /// <param name="onFailedCallback">failed delegate</param>
        public void Login(string username, string password, StringCallback onSuccessCallback, StringCallback onFailedCallback)
        {
            if (Application.internetReachability == NetworkReachability.NotReachable)
            {
                onFailedCallback?.Invoke(Localization.Instance.Get("error_nointernet"));
                return;
            }

            if (string.IsNullOrEmpty(username) ||
                string.IsNullOrEmpty(password) ||
                password.Length < 8)
            {
                onFailedCallback?.Invoke(Localization.Instance.Get("error_login_incorrectdata"));
                return;
            }
            else if (username.Length < 6)
            {
                onFailedCallback?.Invoke(Localization.Instance.Get("error_register_shortname"));
                return;
            }
            else if (username.Contains(" ") || password.Contains(" "))
            {
                onFailedCallback?.Invoke(Localization.Instance.Get("error_register_whitespaces"));
                return;
            }
            IGA_LoginInterface.Login(username, password, onSuccessCallback, onFailedCallback);
        }

        /// <summary>
        /// User login by google method
        /// </summary>
        /// <param name="onSuccessCallback">success delegate</param>
        /// <param name="onFailedCallback">failed delegate</param>
        public void LoginWithGoogle(StringCallback onSuccessCallback, StringCallback onFailedCallback)
        {
            if (Application.internetReachability == NetworkReachability.NotReachable)
            {
                onFailedCallback?.Invoke(Localization.Instance.Get("error_nointernet"));
                return;
            }

            IGA_LoginInterface.LoginGoogle(onSuccessCallback, onFailedCallback);
        }

        /// <summary>
        /// User login by Facebook method
        /// </summary>
        /// <param name="onSuccessCallback">success delegate</param>
        /// <param name="onFailedCallback">failed delegate</param>
        public void LoginWithFacebook(StringCallback onSuccessCallback, StringCallback onFailedCallback)
        {
            if (Application.internetReachability == NetworkReachability.NotReachable)
            {
                onFailedCallback?.Invoke(Localization.Instance.Get("error_nointernet"));
                return;
            }

            IGA_LoginInterface.LoginFacebook(onSuccessCallback, onFailedCallback);
        }

        /// <summary>
        /// Register 
        /// </summary>
        /// <param name="username">username</param>
        /// <param name="password">password</param>
        /// <param name="fullname">fullname</param>
        /// <param name="email">email</param>
        /// <param name="onSuccessCallback">success delegate</param>
        /// <param name="onFailedCallback">failed delegate</param>
        public void RegisterUser(string username, string password, string fullname, string email,
            StringCallback onSuccessCallback, StringCallback onFailedCallback)
        {
            if (Application.internetReachability == NetworkReachability.NotReachable)
            {
                onFailedCallback?.Invoke(Localization.Instance.Get("error_nointernet"));
                return;
            }

            if (string.IsNullOrEmpty(fullname) ||
                string.IsNullOrEmpty(username) ||
                string.IsNullOrEmpty(password) ||
                string.IsNullOrEmpty(email)
            )
            {
                onFailedCallback?.Invoke(Localization.Instance.Get("error_register_no_info"));
            }
            else if (password.Length < 8)
            {
                onFailedCallback?.Invoke(Localization.Instance.Get("error_register_password"));
            }
            else if (!Regex.IsMatch(email, @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"))
            {
                onFailedCallback?.Invoke(Localization.Instance.Get("error_register_invalid_email"));
            }
            else if (username.Contains(" ") || password.Contains(" "))
            {
                onFailedCallback?.Invoke(Localization.Instance.Get("error_register_whitespaces"));
                return;
            }
            else if (username.Length < 6)
            {
                onFailedCallback?.Invoke(Localization.Instance.Get("error_register_shortname"));
                return;
            }
            else
            {
                IGA_LoginInterface.Register(
                    username,
                    password,
                    fullname,
                    email,
                    onSuccessCallback,
                    onFailedCallback
                );
            }
        }

        /// <summary>
        /// Forgot password
        /// </summary>
        /// <param name="email">email</param>
        /// <param name="onSuccessCallback">success delegate</param>
        /// <param name="onFailedCallback">failed delegate</param>
        public void ForgotUserPassword(string email, StringCallback onSuccessCallback, StringCallback onFailedCallback)
        {
            if (Application.internetReachability == NetworkReachability.NotReachable)
            {
                onFailedCallback?.Invoke(Localization.Instance.Get("error_nointernet"));
                return;
            }

            if (string.IsNullOrEmpty(email))
            {
                onFailedCallback?.Invoke(Localization.Instance.Get("error_login_different_method"));
            }
            else if (!Regex.IsMatch(email, @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"))
            {
                onFailedCallback?.Invoke(Localization.Instance.Get("error_forgot_invalidemail"));
            }
            else
            {
                IGA_LoginInterface.ForgotPasswordPassword(email, onSuccessCallback, onFailedCallback);
            }
        }

        /// <summary>
        /// Sets the localization for logon module.
        /// </summary>
        public void SetLocalization(string langKey)
        {
            if (string.IsNullOrEmpty(langKey)) { return; }

            Localization.Instance.SetLanguage(langKey);
        }

        /// <summary>
        /// Gets the user details. 
        /// </summary>
        /// <param name="callback">Callback.</param>
        public void GetUserDetails(ObjectCallback successDelegate,
            StringCallback failedDelegate)
        {
            if (Application.internetReachability == NetworkReachability.NotReachable)
            {
                failedDelegate?.Invoke(Localization.Instance.Get("error_nointernet"));
                return;
            }

            if (UserDetails != null)
            {
                successDelegate?.Invoke(UserDetails);
                return;
            }

            IGA_LoginInterface.GetUserData(
                (object obj) =>
                {
                    //Debug.Log("get user data success: " + ((IGA_Login.Internal.Requests.UserDetails)obj).email);
                    Debug.Log("Get user data success");
                    UserDetails = (Internal.Requests.UserDetails)obj;
                    successDelegate?.Invoke(UserDetails);
                },
                (string message) =>
                {
                    //Debug.Log("get user data failed: " + message);
                    Debug.Log("Get user data success");
                    failedDelegate?.Invoke(null);
                }
            );
        }

        public void ShowReferralCodeInput(Action<bool, string> successDelegate, Action failedDelegate)
        {
            if (tokenManager.IsGuestUser() == true)
            {
                Debug.LogError("Please register user first");
                return;
            }

            if (UserDetails == null)
            {
                Debug.LogError("Please logged in first");
                return;
            }
            if (!string.IsNullOrEmpty(UserDetails.referred_by))
            {
                Debug.LogError("This user redeemed code before");
                return;
            }

            CreateReferralCodeModuleObject();

            referralModuleComponent.ShowReferralCodePanel((isSkipped, input) =>
           {
               successDelegate?.Invoke(isSkipped, input);
           }, () =>
           {
               failedDelegate?.Invoke();
           });

        }

        public void RenewAuthToken(StringCallback successDelegate, StringCallback failedDelegate)
        {
            if (Application.internetReachability == NetworkReachability.NotReachable)
            {
                failedDelegate?.Invoke("Please connect internet");
                return;
            }

            IGA_LoginInterface.TokenRenew((message) =>
           {
               successDelegate?.Invoke("Success");
           }, (message) =>
           {
               failedDelegate?.Invoke(message);
           });
        }

        #region private members 

        private static LoginManager _instance;

        private IGA_LoginModule loginModuleComponent;
        private IGA_ReferralCodeModule referralModuleComponent;

        private void CreateLoginModuleObject()
        {
            if (loginModuleComponent == null)
            {
                GameObject _loginCanvas = Resources.Load("Prefabs/LoginModule", typeof(GameObject)) as GameObject;
                loginModuleComponent = GameObject.Instantiate(_loginCanvas).GetComponent<IGA_LoginModule>();
            }
        }

        private void CreateReferralCodeModuleObject()
        {
            if (referralModuleComponent == null)
            {
                GameObject _referralCanvas = Resources.Load("Prefabs/ReferralModule", typeof(GameObject)) as GameObject;
                referralModuleComponent = GameObject.Instantiate(_referralCanvas).GetComponent<IGA_ReferralCodeModule>();
            }
        }
        #endregion //private members 

        private void GetUserDetails(SimpleCallback successDelegate, SimpleCallback failedDelegate)
        {
            IGA_LoginInterface.GetUserData(
                (object obj) =>
                {
                    //Debug.Log("get user data success: " + ((IGA_Login.Internal.Requests.UserDetails)obj).email);
                    Debug.Log("Get user data success");
                    UserDetails = (Internal.Requests.UserDetails)obj;
                    successDelegate?.Invoke();
                },
                (string message) =>
                {
                    //Debug.Log("get user data failed: " + message);
                    Debug.Log("Get user data failed");
                    failedDelegate?.Invoke();
                }
            );
        }
    }
}