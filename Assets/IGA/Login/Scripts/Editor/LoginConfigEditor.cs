﻿using UnityEditor;
using UnityEngine;
using UnityEngine.Windows;

public class LoginConfigEditor : MonoBehaviour
{
    [MenuItem("HotPlay/Create Login Config")]
    public static LoginConfig Create()
    {
        string path = Application.dataPath + "/IGA/Login/Config";
        if (!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);
        }
        LoginConfig asset = ScriptableObject.CreateInstance<LoginConfig>();
        AssetDatabase.CreateAsset(asset, "Assets/IGA/Login/Config/LoginConfig.asset");
        AssetDatabase.SaveAssets();
        return asset;
    }
}
