﻿using UnityEngine;
using System.Collections.Generic;
using Google;
using Debug = UnityEngine.Debug;
using System.Threading.Tasks;
using HotPlay;

public delegate void StringCallback(string message);

public class GoogleConector : MonoBehaviour
{
    /// <summary>
    /// Reverse web client id for add to info.plish
    /// </summary>
    public const string ReverseWebClientIdIOS = "com.googleusercontent.apps.768321683801-7nib1ad0659ntdqgptctbf8ppqkkovog";

    public LoginConfig Config;
    private GoogleRequestCallbacks _callbacks;
    private GoogleSignInConfiguration configuration;

    public void LoginGoogle(object obj)
    {
        _callbacks = obj as GoogleRequestCallbacks;
        LoginGoogle();
    }

    public void LoginGoogle()
    {
        InitGoogleLoginFlow();
    }

    public void LogoutGoogle()
    {
        Debug.Log("LogoutGoogle LogoutGoogle");
        GoogleSignIn.DefaultInstance.SignOut();
        GoogleSignIn.Configuration = null;
        GoogleSignIn.DefaultInstance.DestroyInstance();
        Debug.Log("LogoutGoogle LogoutGoogle \n\n\n\n\n\n\n");
    }

    private void InitGoogleLoginFlow()
    {
        string webClientId = string.Empty;

        if (Config == null)
        {
            Debug.LogError($"Please assign app id on LoginConfig");
            return;
        }

        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {

            Debug.Log($"Google connector app id : {Config.webClientId_IOS}");
            webClientId = Config.webClientId_IOS;
        }
        else if (Application.platform == RuntimePlatform.Android)
        {
            Debug.Log($"Google connector app id : {Config.webClientId_Android}");
            webClientId = Config.webClientId_Android;
        }
        else
        {
            Debug.LogError("GoogleConector platform :: " + Application.platform);
            Debug.LogError("GoogleConector platform This platform is not supported");
        }

        if (configuration == null)
        {
            configuration = new GoogleSignInConfiguration
            {
                WebClientId = webClientId,
                RequestIdToken = true,
                HidePopups = false,
                UseGameSignIn = false,
                ForceTokenRefresh = true,
                RequestEmail = true,
                RequestProfile = true
            };
            GoogleSignIn.Configuration = configuration;
        }
        GoogleSignIn.DefaultInstance.SignIn().ContinueWith(OnAuthenticationFinished);
    }

    private void OnAuthenticationFinished(Task<GoogleSignInUser> task)
    {
        if (task.IsFaulted)
        {
            using (IEnumerator<System.Exception> enumerator =
                    task.Exception.InnerExceptions.GetEnumerator())
            {
                if (enumerator.MoveNext())
                {
                    Debug.Log("On Authentication ERROR: " + enumerator.Current.Message);
                }
                else
                {
                    Debug.Log("On Authentication ERROR: " + enumerator.Current.Message);
                }
            }

            _callbacks.failedDelegate("error");
            _callbacks = null;
            return;
        }
        else if (task.IsCanceled)
        {
            _callbacks.failedDelegate("cancelled");
            _callbacks = null;
            return;
        }
        else
        {
            _callbacks.successDelegate(task.Result.IdToken);
            _callbacks = null;
            return;
        }
    }
}

