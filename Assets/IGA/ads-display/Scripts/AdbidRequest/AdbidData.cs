﻿using System;

using HotPlay.Ads;
using HotPlay.Ads.Impression;

namespace HotPlay.Adbid
{
    [Serializable]
    public class AdbidData
    {
        public string contract_token;

        public string type { get; set; }
        public string object_slug;
        public string adspace_slug;
        public string coupon_slug;
        public string company_id;
        public string game_slug;

        public ImpressionAdsData th;

        public TranslationsAds translations;

        public AdbidData()
        {
            th = new ImpressionAdsData();
            translations = new TranslationsAds();
        }
    }
}
