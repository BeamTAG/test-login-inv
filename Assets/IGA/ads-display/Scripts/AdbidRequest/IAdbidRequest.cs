﻿using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using UnityEngine;

namespace HotPlay.Adbid
{
    public interface IAdbidRequest
    {
        Task<WebRequestCodeAndMessage> RequestAdbidAdspaceData(string companyId, string gameName, string objectName);
        Task<WebRequestCodeAndMessage> RequestAdbidAdspaceData(string companyId, string gameName, string objectName, string language);
        Task<Texture2D> RequestAdbidAdspaceTexture(string url);

    }
}