using System.Threading.Tasks;
using UnityEngine;

namespace HotPlay.Adbid
{
    public class Mockup : IAdbidRequest
    {
        public async Task<WebRequestCodeAndMessage> RequestAdbidAdspaceData(string companyId, string gameName, string objectName)
        {
            var task = await Task<WebRequestCodeAndMessage>.Factory.StartNew(() =>
            {
                var data = new WebRequestCodeAndMessage();
                data.code = 200;
                data.message =
                    "[{\"contract_token\": \"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0aW1lc3RhbXAiOiIyMDE5LTA0LTEwVDA2OjIwOjU4LjUxMzM1NCIsImV4cCI6MTU1NDg3ODE1OCwiaWRlbnRpdHlfaWQiOiIxNTU0ODE1NjQ0LUM1SWNINmMtNVc3SVdNQzhiN08zNG9WRGtSRTdrUWVuIiwiYmlkX29mZmVyIjowLjAsImFkX3R5cGUiOiJJTVBSRVNTSU9OIiwiY29tcGFueV9pZCI6IjdiNTkxNWY4LTA4YWUtNDNmNi05YTBjLTY5MmYwYmU5NDk5ZiIsImdhbWVfc2x1ZyI6ImVoLWdhbWUiLCJvYmplY3Rfc2x1ZyI6ImZpc2hpbmctYmFubmVyIiwiYWRzcGFjZV9zbHVnIjoiYmFubmVyLWFkc3BhY2UiLCJjYW1wYWlnbl9hZHZlcnRpc2VyX3NsdWciOiJoaXAtYWRtaW4iLCJjYW1wYWlnbl9icmFuZF9zbHVnIjoia2l0a2F0IiwiY2FtcGFpZ25fcHJvZHVjdF9zbHVnIjoia2l0a2F0LWJhciIsImNhbXBhaWduX3NsdWciOiJraXRrYXQtYmFubmVyMi0yMDE5In0.eO-2cjDWkHPxx_ZZBSKPMgmfq6mRci0PXHwuv_aYw6c\"," +
                    "\"company_id\": \"7b5915f8-08ae-43f6-9a0c-692f0be9499f\", " +
                    "\"game_slug\": \"eh-game\"," +
                    "\"object_slug\": \"fishing-banner\", " +
                    "\"adspace_slug\": \"banner-adspace\"," +
                    "\"th\": {\"adSpaceObjectName\": \"\", " +
                    "\"saved_ontemp\": false, " +
                    "\"texture2D\": {\"m_FileID\": -276, \"m_PathID\": 0}," +
                    "\"textureColor\": {\"a\": 1, \"b\": 1, \"g\": 1, \"r\": 1}, " +
                    "\"textureID\": \"\", " +
                    "\"texturePosition\": {\"x\": 0, \"y\": 0}," +
                    "\"textureScale\": {\"x\": 1, \"y\": 1}, " +
                    "\"textureURL\": \"https://hip.alpha.hot-now.com/media/download/07004295a71f000f65b8cce9c8507b812db31b2d5423e65c43b38ba80ba678e8.FABwBClacfAA9luMzpyFB7gS2zGy1UI-ZcQ7OLqAumeOg.jpg\", " +
                    "\"type\": 0}}, " +
                    "]";

                return data;
            });
            return task;
        }

        public async Task<WebRequestCodeAndMessage> RequestAdbidAdspaceData(string companyId, string gameName, string objectName, string language)
        {
            var task = await Task<WebRequestCodeAndMessage>.Factory.StartNew(() =>
            {
                var data = new WebRequestCodeAndMessage();
                data.code = 200;
                data.message =
                    "[{\"contract_token\": \"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0aW1lc3RhbXAiOiIyMDE5LTA0LTEwVDA2OjIwOjU4LjUxMzM1NCIsImV4cCI6MTU1NDg3ODE1OCwiaWRlbnRpdHlfaWQiOiIxNTU0ODE1NjQ0LUM1SWNINmMtNVc3SVdNQzhiN08zNG9WRGtSRTdrUWVuIiwiYmlkX29mZmVyIjowLjAsImFkX3R5cGUiOiJJTVBSRVNTSU9OIiwiY29tcGFueV9pZCI6IjdiNTkxNWY4LTA4YWUtNDNmNi05YTBjLTY5MmYwYmU5NDk5ZiIsImdhbWVfc2x1ZyI6ImVoLWdhbWUiLCJvYmplY3Rfc2x1ZyI6ImZpc2hpbmctYmFubmVyIiwiYWRzcGFjZV9zbHVnIjoiYmFubmVyLWFkc3BhY2UiLCJjYW1wYWlnbl9hZHZlcnRpc2VyX3NsdWciOiJoaXAtYWRtaW4iLCJjYW1wYWlnbl9icmFuZF9zbHVnIjoia2l0a2F0IiwiY2FtcGFpZ25fcHJvZHVjdF9zbHVnIjoia2l0a2F0LWJhciIsImNhbXBhaWduX3NsdWciOiJraXRrYXQtYmFubmVyMi0yMDE5In0.eO-2cjDWkHPxx_ZZBSKPMgmfq6mRci0PXHwuv_aYw6c\"," +
                    "\"company_id\": \"7b5915f8-08ae-43f6-9a0c-692f0be9499f\", " +
                    "\"game_slug\": \"eh-game\"," +
                    "\"object_slug\": \"fishing-banner\", " +
                    "\"adspace_slug\": \"banner-adspace\"," +
                    "\"th\": {\"adSpaceObjectName\": \"\", " +
                    "\"saved_ontemp\": false, " +
                    "\"texture2D\": {\"m_FileID\": -276, \"m_PathID\": 0}," +
                    "\"textureColor\": {\"a\": 1, \"b\": 1, \"g\": 1, \"r\": 1}, " +
                    "\"textureID\": \"\", " +
                    "\"texturePosition\": {\"x\": 0, \"y\": 0}," +
                    "\"textureScale\": {\"x\": 1, \"y\": 1}, " +
                    "\"textureURL\": \"https://hip.alpha.hot-now.com/media/download/07004295a71f000f65b8cce9c8507b812db31b2d5423e65c43b38ba80ba678e8.FABwBClacfAA9luMzpyFB7gS2zGy1UI-ZcQ7OLqAumeOg.jpg\", " +
                    "\"type\": 0}}, " +
                    "]";

                return data;
            });

            return task;
        }

        public async Task<Texture2D> RequestAdbidAdspaceTexture(string url)
        {
            var task = Task<Texture2D>.Factory.StartNew(() =>
            {
                var data = new Texture2D(50, 50);

                return data;
            });
            return task.GetAwaiter().GetResult();
        }
    }
}