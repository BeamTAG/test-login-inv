using System.Threading.Tasks;
using UnityEngine;

namespace HotPlay.Adbid {

    public class WebRequest : IAdbidRequest {
        GenericWebRequest webRequest = new GenericWebRequest ();

        public async Task<WebRequestCodeAndMessage> RequestAdbidAdspaceData (string companyId, string gameName, string objectName) {
            var url = API.RequestConfig.GET_ADBID_ADSPACE (companyId, gameName, objectName);
            var request = await webRequest.Get (url);
            return webRequest.GetResponseFromRequest (request);
        }

        public async Task<WebRequestCodeAndMessage> RequestAdbidAdspaceData (string companyId, string gameName, string objectName, string language) {
            var url = API.RequestConfig.GET_ADBID_ADSPACE (companyId, gameName, objectName, language);
            var request = await webRequest.Get (url);
            return webRequest.GetResponseFromRequest (request);
        }

        public async Task<Texture2D> RequestAdbidAdspaceTexture (string url) {
            return await webRequest.GetTexture (url);
        }

    }
}