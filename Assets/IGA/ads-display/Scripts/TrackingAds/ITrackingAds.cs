﻿using System.Threading.Tasks;

namespace HotPlay.Tracking
{
    public interface ITrackingAds
    {
        Task<WebRequestCodeAndMessage> RequestTrackingAds(string contract_token);
    }
}
