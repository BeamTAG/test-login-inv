﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HotPlay.Tracking
{
    [Serializable]
    public class ReservationPayload
    {
        public string executed;
        public ReservationProof proof;
    }
}
