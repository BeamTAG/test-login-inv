﻿using System.Threading.Tasks;

namespace HotPlay.Tracking
{
    public class Mockup : ITrackingAds
    {
        public async Task<WebRequestCodeAndMessage> RequestTrackingAds(string contract_token)
        {
            var task = Task<WebRequestCodeAndMessage>.Factory.StartNew(() =>
            {
                var data = new WebRequestCodeAndMessage();
                data.code = 200;
                data.message = "{\"created\":\"2019-04-24 03:40:47.232439+00\",\"executed\":\"2019-04-24 03:40:47+00\",\"proof\":{},\"reservation\":\"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0aW1lc3RhbXAiOiIyMDE5LTA0LTI0VDAzOjQwOjQ3LjAxMzQyNSIsImV4cCI6MTU1NjA3ODE0NywiaWRlbnRpdHlfaWQiOiIxNTU1OTE3OTQxLU41bnNqT3Q4aEVNdjlYaTQtYjFrdW8wa0k1Y1VacGFsIiwiYmlkX29mZmVyIjowLjAsImFkX3R5cGUiOiJJTVBSRVNTSU9OIiwiY29tcGFueV9pZCI6IjdiNTkxNWY4LTA4YWUtNDNmNi05YTBjLTY5MmYwYmU5NDk5ZiIsImdhbWVfc2x1ZyI6ImVoLWdhbWUiLCJvYmplY3Rfc2x1ZyI6ImV2ZXJob3QtbWFjaGluZS1iYW5uZXIiLCJhZHNwYWNlX3NsdWciOiJiYW5uZXItYWRzcGFjZSIsImNhbXBhaWduX2FkdmVydGlzZXJfc2x1ZyI6ImhpcC1hZG1pbiIsImNhbXBhaWduX2JyYW5kX3NsdWciOiJraXRrYXQiLCJjYW1wYWlnbl9wcm9kdWN0X3NsdWciOiJraXRrYXQtYmFyIiwiY2FtcGFpZ25fc2x1ZyI6ImtpdGthdC1iYW5uZXIyLTIwMTkifQ.tCEGoWwaFhPsGO3zdvtMAdV0_PTNtLaJNP6OoyBIuuA\"}";

                return data;
            });
            return task.GetAwaiter().GetResult();
        }
    }
}
