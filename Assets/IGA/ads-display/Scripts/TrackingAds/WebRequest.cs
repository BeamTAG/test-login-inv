﻿using System;
using System.Globalization;
using System.Threading.Tasks;
using UnityEngine;

namespace HotPlay.Tracking
{
    public class WebRequest : ITrackingAds
    {
        GenericWebRequest webRequest = new GenericWebRequest();

        public async Task<WebRequestCodeAndMessage> RequestTrackingAds(string contract_token)
        {
            var url = HotPlay.API.RequestConfig.PUT_ADTRACK_RESERVATION(contract_token);

            var dt = DateTime.Now.ToString("yyyy-MM-dd HH:mm:sszzz", CultureInfo.InvariantCulture);

            var reservation_data = new ReservationPayload
            {
                executed = dt,
                proof = new ReservationProof()
            };

            var json_data = JsonUtility.ToJson(reservation_data);

            var request = await webRequest.Put(url, json_data);
            return webRequest.GetResponseFromRequest(request);
        }
    }
}
