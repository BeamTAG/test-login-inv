
namespace HotPlay.Internal
{
    class AppData<T>
    {
        public T app_data;

        public AppData(T t)
        {
            app_data = t;
        }
    }
}