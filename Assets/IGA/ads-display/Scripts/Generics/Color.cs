using System;

namespace HotPlay.core
{
    [Serializable]
    public class Color
    {
        public float a;
        public float b;
        public float g;
        public float r;

        public Color() { }
        public Color(float _r, float _g, float _b, float _a)
        {
            r = _r;
            g = _g;
            b = _b;
            a = _a;
        }
    }
}

