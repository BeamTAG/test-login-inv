using System;

namespace HotPlay.core
{
    [Serializable]
    public class Vector2
    {
        public float x;
        public float y;

        public Vector2() { }
        public Vector2(float _x, float _y)
        {
            x = _x;
            y = _y;
        }
    }

}
