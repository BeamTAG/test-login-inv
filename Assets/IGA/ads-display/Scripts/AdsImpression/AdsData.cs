﻿using System;

namespace HotPlay.Ads.Impression
{
    [Serializable]
    public abstract class AdsData
    {
        public string adSpaceObjectName;
        public bool saved_ontemp = false;

        public AdsData() { }

        abstract public AdsData getData();
    }
}
