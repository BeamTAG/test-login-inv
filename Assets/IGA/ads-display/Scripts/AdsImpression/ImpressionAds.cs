﻿using System;

namespace HotPlay.Ads.Impression
{
    [Serializable]
    public class ImpressionAds
    {
        public string contract_token;

        public string object_slug;
        public string adspace_slug;
        public ImpressionAdsData configs = new ImpressionAdsData();

        public ImpressionAds()
        {

        }

        public ImpressionAds(string contract_token, ImpressionAdsData configs)
        {
            this.contract_token = contract_token;
            this.configs = configs;
        }
    }
}
