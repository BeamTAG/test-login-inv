﻿using System;
using UnityEngine;

namespace HotPlay.Ads.Impression
{
    [Serializable]
    public class ImpressionAdsData : AdsData
    {
        public string textureID;
        public string textureURL;
        public Texture2D texture2D;
        public HotPlay.core.Vector2 texturePosition = new HotPlay.core.Vector2(0f, 0f);
        public HotPlay.core.Vector2 textureScale = new HotPlay.core.Vector2(1f, 1f);
        public HotPlay.core.Color Color = new HotPlay.core.Color(1f, 1f, 1f, 1f);

        public string deal_id;
        public string issue_app;
        public string deal;

        //Constructor
        public ImpressionAdsData()
        {
        }
        public ImpressionAdsData(AdsData data, string _id, string _url, Texture2D _2d, HotPlay.core.Vector2 _scale, HotPlay.core.Vector2 _pos, HotPlay.core.Color _col)
        {
            this.adSpaceObjectName = data.adSpaceObjectName;
            this.textureID = _id;
            this.textureURL = _url;
            this.texture2D = _2d;
            this.textureScale = _scale;
            this.texturePosition = _pos;
            this.Color = _col;
        }

        public override AdsData getData()
        {
            return this;
        }
    }
}
