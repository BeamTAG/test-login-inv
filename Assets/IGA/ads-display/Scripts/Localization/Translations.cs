using System;
using UnityEngine;

using HotPlay.Ads.Impression;
using HotPlay.Ads.Coupon;


namespace HotPlay.Ads
{
    [Serializable]
    public class TranslationsAds
    {
        [SerializeField] public ImpressionAdsData en;
        [SerializeField] public ImpressionAdsData th;

        public TranslationsAds()
        {
            en = new ImpressionAdsData();
            th = new ImpressionAdsData();
        }
    }

    [Serializable]
    public class TranslationsCoupon
    {
        [SerializeField] public CouponDataWithoutTranslation en;
        [SerializeField] public CouponDataWithoutTranslation th;

        public TranslationsCoupon()
        {
            en = new CouponDataWithoutTranslation();
            th = new CouponDataWithoutTranslation();
        }
    }

}