namespace HotPlay.Ads
{
    public static class Localization
    {
        public class Language
        {
            public const string en = "en";
            public const string th = "th";
        }

        public static string GetLocalization(string lang)
        {
            switch (lang)
            {
                case Language.th:
                    return "th";
                case Language.en:
                    return "en";
                default:
                    return "en";
            }
        }

        public static string WithReplaceLanguageToLowerCase(this string message)
        {
            message = message.Replace("\"TH\":", "\"th\":");
            message = message.Replace("\"EN\":", "\"en\":");
            return message;
        }
    }
}