#define DEVELOP
#define DEBUG

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Task = System.Threading.Tasks.Task;

using HotPlay.Adbid;
using HotPlay.Ads;
using HotPlay.Ads.Coupon;
using HotPlay.Ads.Impression;
using HotPlay.Internal;
using HotPlay.Tracking;

namespace HotPlay
{

    /// <summary>
    /// Instance this class for use adspace tools
    /// </summary>
    public class AdsManager : MonoBehaviour
    {
        #region Constructor

        public AdsManager()
        {
            AdbidRequest = new Adbid.WebRequest();
            CouponRequest = new Ads.Coupon.WebRequest();
            TrackingAds = new Tracking.WebRequest();
        }

        public void AdsManagerConstructor(IAdbidRequest _adbidRequest, ICouponRequest _couponRequest, ITrackingAds _trackingAds)
        {
            AdbidRequest = _adbidRequest;
            CouponRequest = _couponRequest;
            TrackingAds = _trackingAds;
        }

        #endregion

        #region Public members

        public string CompanyId
        {
            get { return _companyId; }
            private set { _companyId = value; }
        }

        public string GameName
        {
            get { return _gameName; }
            private set { _gameName = value; }
        }

        public string Language
        {
            get { return _language; }
            private set { _language = value; }
        }

        #endregion

        #region private members

        private static AdsManager _instance;
        private string _companyId;
        private string _gameName;
        private string _language = Localization.Language.th;

        private bool isLoadAssetBundle = false;

        private IAdbidRequest adbidRequest;
        private ICouponRequest couponRequest;
        private ITrackingAds trackingAdsRequest;

        #endregion

        #region Public method

        /// <summary>
        /// returns the instance of AdsManager.
        /// </summary>
        /// <returns>instance AdsManager</returns>
        public static AdsManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    var go = new GameObject("AdsManager");
                    _instance = go.AddComponent<AdsManager>();
                    DontDestroyOnLoad(go);
                }

                return _instance;
            }
        }

        private IAdbidRequest AdbidRequest
        {
            get { return adbidRequest; }
            set { adbidRequest = value; }
        }

        private ICouponRequest CouponRequest
        {
            get { return couponRequest; }
            set { couponRequest = value; }
        }

        private ITrackingAds TrackingAds
        {
            get { return trackingAdsRequest; }
            set { trackingAdsRequest = value; }
        }

        /// <summary>
        /// Get all andCouponData and assets from server (For EH Launch)
        /// </summary>
        /// <param name="companyId">Company UID</param>
        /// <param name="gameName">Game Name</param>
        /// <param name="callback">Callback(bool, string)callback</param>
        public void InitializeGame(
            string companyId,
            string gameName,
            string baseBackendURL,
            Action<bool, string> onInitializeDone)
        {
            if (string.IsNullOrEmpty(companyId) ||
                string.IsNullOrEmpty(gameName) ||
                string.IsNullOrEmpty(baseBackendURL))
            {
                throw new ArgumentNullException($"Please assign companyId/gameName/baseBackendURL before initialize game");
            }

            InitializeGame(companyId, gameName, baseBackendURL, Localization.Language.th, onInitializeDone);
        }

        public void InitializeGame(
            string companyId,
            string gameName,
            string baseBackendUrl,
            string language,
            Action<bool, string> onInitializeDone)
        {
            if (!string.IsNullOrEmpty(baseBackendUrl))
            {
                API.RequestConfig.SERVERURL = baseBackendUrl;
            }

            CompanyId = companyId;
            GameName = gameName;
            Language = language;

            onInitializeDone?.Invoke(true, "Success");
        }

        public Task GetImpressionAds(string objectName, Action<ImpressionAds> OnLoadSuccess,
            Action<string, string> OnLoadFailed)
        {
            if (string.IsNullOrEmpty(CompanyId) ||
                string.IsNullOrEmpty(GameName)
            )
            {
                throw new NullReferenceException($"Please init method {nameof(InitializeGame)} first");
            }

            if (string.IsNullOrEmpty(objectName))
            {
                throw new ArgumentNullException($"objectName cannot null");
            }

            return GetImpressionAdsInternal(objectName, OnLoadSuccess, OnLoadFailed);
        }

        public Task GetCouponWithImpressionAds(string objectName, Action<CouponDataWithImpressionAds> OnSuccess,
            Action<string, string> OnFailed)
        {
            if (string.IsNullOrEmpty(CompanyId) ||
                string.IsNullOrEmpty(GameName)
            )
            {
                throw new NullReferenceException($"Please init method {nameof(InitializeGame)} first");
            }

            if (string.IsNullOrEmpty(objectName))
            {
                throw new ArgumentNullException($"objectName cannot null");
            }

            return GetCouponWithImpressionAdsInternal(objectName, OnSuccess, OnFailed);
        }

        public Task GetCouponWithoutImpressionAds(string objectName, Action<CouponData> OnLoadSuccess,
            Action<string, string> OnLoadFailed)
        {
            if (string.IsNullOrEmpty(CompanyId) ||
                string.IsNullOrEmpty(GameName)
            )
            {
                throw new NullReferenceException($"Please init method {nameof(InitializeGame)} first");
            }

            if (string.IsNullOrEmpty(objectName))
            {
                throw new ArgumentNullException($"objectName cannot null");
            }

            return GetCouponWithoutImpressionAdsInternal(objectName, OnLoadSuccess, OnLoadFailed);
        }

        public Task TrackAdActivity(string contract_token, Action<string, string> OnSuccessCallback,
            Action<string, string> OnFailedCallback)
        {

            if (string.IsNullOrEmpty(contract_token))
            {
                throw new ArgumentNullException($"Please input contract_token in {nameof(TrackAdActivity)}");
            }

            return TrackAdActivityInternal(contract_token, OnSuccessCallback, OnFailedCallback);
        }

        private async Task GetImpressionAdsInternal(string objectName, Action<ImpressionAds> OnLoadSuccess,
            Action<string, string> OnLoadFailed)
        {
            var data = await AdbidRequest.RequestAdbidAdspaceData(CompanyId, GameName, objectName, Language);

            DebugResponse("LoadAdsTexture", data.code.ToString(), data.message);

            if (data.code == 200)
            {
                var adsArray = (AdsProcess(data.message)) as ImpressionAds;

                if (adsArray != null)
                {
                    OnLoadSuccess?.Invoke(adsArray);
                }
                else
                {
                    Debug.Log("Not have ads");
                    OnLoadSuccess?.Invoke(null);
                }
            }
            else
            {
                OnLoadFailed?.Invoke(data.code.ToString(), data.message);
            }
        }

        private async Task GetCouponWithImpressionAdsInternal(string objectName, Action<CouponDataWithImpressionAds> OnSuccess,
            Action<string, string> OnFailed)
        {
            var data = await AdbidRequest.RequestAdbidAdspaceData(CompanyId, GameName, objectName, Language);

            DebugResponse("LoadAds", data.code.ToString(), data.message);

            if (data.code == 200)
            {
                var couponWithImpression = (AdsProcess(data.message)) as ImpressionAds;

                if (couponWithImpression != null)
                {
                    var adsDataWithCoupon = await DownloadCouponData(couponWithImpression);
                    var adsData = new CouponDataWithImpressionAds();
                    adsData.contract_token = couponWithImpression.contract_token;
                    adsData.Impression = couponWithImpression;
                    adsData.couponData = adsDataWithCoupon.couponData;

                    Debug.Log("Load Success");
                    OnSuccess?.Invoke(adsData);
                }
                else
                {
                    Debug.Log("Not have ads");
                    OnSuccess?.Invoke(null);
                }
            }
            else
            {
                OnFailed?.Invoke(data.code.ToString(), data.message);
            }
        }

        private async Task GetCouponWithoutImpressionAdsInternal(string objectName, Action<CouponData> OnLoadSuccess,
            Action<string, string> OnLoadFailed)
        {
            var data = await AdbidRequest.RequestAdbidAdspaceData(CompanyId, GameName, objectName, Language);

            DebugResponse("LoadAdsCoupon", data.code.ToString(), data.message);

            if (data.code == 200)
            {
                var impressionAds = (AdsProcess(data.message)) as ImpressionAds;

                if (impressionAds != null)
                {
                    var adsDataWithCoupon = await DownloadCouponData(impressionAds);

                    OnLoadSuccess?.Invoke(adsDataWithCoupon.couponData);
                }
                else
                {
                    Debug.Log("Not have coupon");
                    OnLoadSuccess?.Invoke(null);
                }
            }
            else
            {
                OnLoadFailed?.Invoke(data.code.ToString(), data.message);
            }
        }

        private async Task TrackAdActivityInternal(string contract_token, Action<string, string> success,
            Action<string, string> failed)
        {
            var data = await trackingAdsRequest.RequestTrackingAds(contract_token);

            DebugResponse("Tracking ads", data.code.ToString(), data.message);

            if (data.code == 200)
            {
                success?.Invoke(data.code.ToString(), data.message);
            }
            else
            {
                failed?.Invoke(data.code.ToString(), data.message);
            }
        }

        #endregion// Public members

        #region Private members

        private object AdsProcess(string message)
        {
            if (!string.IsNullOrEmpty(message))
            {
                var data = DeserializeJsonDataFromResponse(message);

                if (data != null)
                {
                    var dataArrayFromAdbid = ChangeJsonDataToCustomizeAdspaceData(data);

                    if (data != null)
                    {
                        return dataArrayFromAdbid;
                    }
                }
            }

            return null;
        }

        private async Task<bool> DownloadAdsTextureInArray(IEnumerable<ImpressionAds> dataArray)
        {
            foreach (var data in dataArray)
            {
                var texture = await AdbidRequest.RequestAdbidAdspaceTexture(data.configs.textureURL);
                data.configs.texture2D = texture;
            }

            Debug.Log("Download all textures success");
            return true;
        }

        private async Task<bool> DownloadCouponTextureInArray(CouponData data)
        {
            var texture = await AdbidRequest.RequestAdbidAdspaceTexture(data.image_url);
            data.coupon_texture = texture;

            Debug.Log("Download all textures success");
            return true;
        }

        private async Task<CouponDataWithImpressionAds> DownloadCouponData(ImpressionAds impression)
        {
            if (!string.IsNullOrEmpty(impression.configs.deal))
            {
                var url = AddEndpointToCouponLink(impression.configs.deal);
                var couponData = await CouponRequest.RequestGetCouponData(url);
                if (couponData.code == 200)
                {
                    if (string.IsNullOrEmpty(couponData.message)) return null;

                    var deserializeCouponData =
                        JsonUtility.FromJson<AppData<CouponData>>(couponData.message);

                    var couponFromLangParam = GetCouponByLanguage(deserializeCouponData.app_data);
                    couponFromLangParam.contract_token = impression.contract_token;

                    if (!string.IsNullOrEmpty(couponFromLangParam.image_url))
                    {
                        var returnAds = new CouponDataWithImpressionAds();
                        returnAds.Impression = impression;
                        returnAds.couponData = couponFromLangParam;

                        return returnAds;
                    }
                }
            }

            return null;
        }

        private string AddEndpointToCouponLink(string deal_url)
        {
            StringBuilder str = new StringBuilder();
            str.Append(API.RequestConfig.SERVERURL);
#if DEVELOP
            str.Replace("hip.", "");
#endif
            str.Append("/inventory");
            str.Append(deal_url);

            return str.ToString();
        }

        #endregion

        private AdbidDataArray DeserializeJsonDataFromResponse(string message)
        {
            var resultMessage = AddFormatToJsonForDeserialize(message);
            var data = JsonUtility.FromJson<AdbidDataArray>(resultMessage);

            if (data?.adbidDataArray == null) return null;
            if (data.adbidDataArray?.Length <= 0) return null;

            var customizeData = data.adbidDataArray[0];
            if (Language == Localization.Language.en && !string.IsNullOrEmpty(customizeData.translations.en.textureURL))
            {
                return data;
            }
            if (Language == Localization.Language.th && !string.IsNullOrEmpty(customizeData.translations.th.textureURL))
            {
                return data;
            }

            return null;
        }

        private string AddFormatToJsonForDeserialize(string message)
        {
            return "{\"adbidDataArray\":" + message.WithReplaceLanguageToLowerCase() + "}";
        }

        private ImpressionAds ChangeJsonDataToCustomizeAdspaceData(
            AdbidDataArray data)
        {
            var dataArrayFromAdbid = new ImpressionAds[data.adbidDataArray.Length];
            for (var i = 0; i < data.adbidDataArray.Length; i++)
            {
                switch (Language)
                {
                    case Localization.Language.en:
                        if (!string.IsNullOrEmpty(data.adbidDataArray[i].translations.en.textureURL))
                        {
                            dataArrayFromAdbid[i] = new ImpressionAds(
                                data.adbidDataArray[i].contract_token,
                                data.adbidDataArray[i].translations.en);
                        }
                        else
                        {
                            dataArrayFromAdbid = null;
                        }

                        break;
                    default:
                        if (!string.IsNullOrEmpty(data.adbidDataArray[i].translations.th.textureURL))
                        {
                            dataArrayFromAdbid[i] = new ImpressionAds(
                                data.adbidDataArray[i].contract_token,
                                data.adbidDataArray[i].translations.th);
                        }
                        else
                        {
                            dataArrayFromAdbid = null;
                        }
                        break;
                }

                if (dataArrayFromAdbid == null)
                {
                    Debug.Log("NO TRANSLATION");
                    dataArrayFromAdbid[i] = new ImpressionAds(
                        data.adbidDataArray[i].contract_token,
                        data.adbidDataArray[i].th);
                }
            }

            return dataArrayFromAdbid[0];
        }

        private CouponData GetCouponByLanguage(CouponData couponData)
        {
            CouponData data = null;
            if (!string.IsNullOrEmpty(couponData.translations.en.image_url) ||
                !string.IsNullOrEmpty(couponData.translations.th.image_url))
            {
                switch (Language)
                {
                    case Localization.Language.en:
                        data = couponData.translations.en.ConvertToIGACoupon();
                        break;
                    case Localization.Language.th:
                        data = couponData.translations.th.ConvertToIGACoupon();
                        break;
                    default:
                        data = couponData.translations.en.ConvertToIGACoupon();
                        break;
                }
            }
            else
            {
                data = couponData;
            }

            return data;
        }

        private void DebugResponse(string funcName, string code, string message)
        {
#if SHOWDEBUG
            Debug.Log($"======== {funcName} Response =======");
            Debug.Log($"Code : {code}");
            Debug.Log($"Message : {message}");
            Debug.Log("=========================");
#else
            try
            {
                int responseCode = int.Parse(code);
                if (responseCode < 300)
                {
                    Debug.Log($"{funcName} Success");

                }
                else
                {
                    Debug.Log($"{funcName} Failed, {code}");
                }
            }
            catch (Exception e)
            {
                Debug.Log("Parse response code error");
            }
#endif
        }
    }
}