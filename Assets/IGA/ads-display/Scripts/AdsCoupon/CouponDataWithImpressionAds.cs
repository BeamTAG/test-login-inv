﻿using HotPlay.Ads.Impression;

namespace HotPlay.Ads.Coupon
{
    public class CouponDataWithImpressionAds
    {
        public string contract_token;
        public ImpressionAds Impression;
        public CouponData couponData;
    }
}