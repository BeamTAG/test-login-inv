﻿using System;
using System.Threading.Tasks;
using HotPlay;

namespace HotPlay.Ads.Coupon
{
    public class Mockup : ICouponRequest
    {
        public async Task<WebRequestCodeAndMessage> RequestGetCouponData(string url)
        {
            var task = Task<WebRequestCodeAndMessage>.Factory.StartNew(() =>
            {
                var data = new WebRequestCodeAndMessage();
                data.code = 200;
                data.message = "{\"app_data\":{\"company_name\":\"merchant011\",\"deal_id\":\"288\",\"deal_type\":\"unlock\",\"description\":\"Free 1 drink.\",\"execution_price\":\"2\",\"expiration_date\":\"2019-06-10 16:59:00+00:00\",\"for_game\":true,\"image_url\":\"https://hip.alpha.hot-now.com/media/download/e23726a950fe20a3a0827f065256415928d5394403dce379ef2819ceedf17707.FA4jcmqVD-IKOggn8GUlZBWSjVOUQD3ON57ygZzu3xdwc.jpg\",\"merchant_id\":\"1552476478-nqcdQRGbBZIbWVF5iYM_8MW_txYwql8E\",\"name\":\"KOI The \",\"need_reserve\":true,\"number_of_coupons\":50,\"reservation_price\":\"2\",\"translations\":{\"th\":{\"company_name\":\"merchant011\",\"deal_id\":\"288\",\"deal_type\":\"unlock\",\"description\":\"ฟรี 1 แก้ว\",\"execution_price\":\"2\",\"expiration_date\":\"2019-06-10 16:59:00+00:00\",\"for_game\":true,\"image_url\":\"https://hip.alpha.hot-now.com/media/download/e23726a950fe20a3a0827f065256415928d5394403dce379ef2819ceedf17707.FA4jcmqVD-IKOggn8GUlZBWSjVOUQD3ON57ygZzu3xdwc.jpg\",\"merchant_id\":\"1552476478-nqcdQRGbBZIbWVF5iYM_8MW_txYwql8E\",\"name\":\"ก้อยเดอะ\",\"need_reserve\":true,\"number_of_coupons\":50,\"reservation_price\":\"2\"},\"en\":{\"company_name\":\"merchant011\",\"deal_id\":\"288\",\"deal_type\":\"unlock\",\"description\":\"Free 1 drink.\",\"execution_price\":\"2\",\"expiration_date\":\"2019-06-10 16:59:00+00:00\",\"for_game\":true,\"image_url\":\"https://hip.alpha.hot-now.com/media/download/e23726a950fe20a3a0827f065256415928d5394403dce379ef2819ceedf17707.FA4jcmqVD-IKOggn8GUlZBWSjVOUQD3ON57ygZzu3xdwc.jpg\",\"merchant_id\":\"1552476478-nqcdQRGbBZIbWVF5iYM_8MW_txYwql8E\",\"name\":\"KOI The \",\"need_reserve\":true,\"number_of_coupons\":50,\"reservation_price\":\"2\"}}},\"issuing_app\":\"hotnow\",\"slug\":\"288\"}";

                return data;
            });
            return task.GetAwaiter().GetResult();
        }

    }
}
