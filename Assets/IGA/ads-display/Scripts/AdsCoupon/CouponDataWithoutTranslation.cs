﻿using System;
using UnityEngine;

namespace HotPlay.Ads.Coupon
{
    [Serializable]
    public class CouponDataWithoutTranslation
    {
        public string contract_token;

        public string id;
        public string name;
        public string description;
        public string image_url;
        public string expiration_date;
        public string company_name;

        public Texture2D coupon_texture;

        public CouponDataWithoutTranslation()
        {
        }

        public CouponData ConvertToIGACoupon()
        {
            return new CouponData(id, name, image_url, description, expiration_date, company_name);
        }
    }
}