﻿using System;
using UnityEngine;

namespace HotPlay.Ads.Coupon
{
    [Serializable]
    public class CouponData
    {
        public string contract_token;

        public string id;
        public string name;
        public string description;
        public string image_url;
        public string expiration_date;
        public string company_name;

        public Texture2D coupon_texture;

        public TranslationsCoupon translations = new TranslationsCoupon();

        public CouponData()
        {
            this.translations = new TranslationsCoupon();
        }

        public CouponData(string id, string name, string image_url, string description, string expirationData, string companyName)
        {
            this.id = id;
            this.name = name;
            this.image_url = image_url;
            this.description = description;
            this.expiration_date = expirationData;
            this.company_name = companyName;
        }
    }
}
