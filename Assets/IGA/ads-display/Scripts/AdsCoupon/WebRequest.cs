﻿using System;
using System.Threading.Tasks;
using HotPlay;

namespace HotPlay.Ads.Coupon {
	public class WebRequest : ICouponRequest {
		GenericWebRequest webRequest = new GenericWebRequest ();

		public async Task<WebRequestCodeAndMessage> RequestGetCouponData (string url) {
			var request = await webRequest.Get (url);
			return webRequest.GetResponseFromRequest (request);
		}
	}
}