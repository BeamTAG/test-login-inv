﻿using UnityEngine;
using System.Collections;
using System.Threading.Tasks;
using HotPlay;

namespace HotPlay.Ads.Coupon
{
    public interface ICouponRequest
    {
        Task<WebRequestCodeAndMessage> RequestGetCouponData(string url);
    }
}

