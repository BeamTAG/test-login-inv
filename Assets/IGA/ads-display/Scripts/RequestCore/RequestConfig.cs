﻿//#define LOCAL

using HotPlay.Strings;
using Localization = HotPlay.Ads.Localization;

namespace HotPlay
{
    namespace API
    {
        public class RequestConfig
        {
            private static string MY_IP = "https://dev.hot-now.com";
            private static string PROTEUS_IP;

            public static string SERVERURL
            {
                get
                {
                    if (string.IsNullOrEmpty(PROTEUS_IP))
                    {
                        return MY_IP;
                    }
                    return PROTEUS_IP;

                }
                set
                {
                    PROTEUS_IP = value;
                }
            }

            public static string GET_ADBID_ADSPACE(string companyId, string gameName, string objectName) =>
                (string.IsNullOrEmpty(SERVERURL) ? MY_IP : SERVERURL) +
                "/adbid" +
                "/get-ads?" +
                "company=" + companyId +
                "&game=" + Slug.Generate(gameName) +
                "&object=" + Slug.Generate(objectName);

            public static string GET_ADBID_ADSPACE(string companyId, string gameName, string objectName,
                string language) =>
                (string.IsNullOrEmpty(SERVERURL) ? MY_IP : SERVERURL) +
                "/adbid" +
                "/get-ads?" +
                "company=" + companyId +
                "&game=" + Slug.Generate(gameName) +
                "&object=" + Slug.Generate(objectName) +
                "&language=" + Localization.GetLocalization(language);

            public static string PUT_ADTRACK_RESERVATION(string contract_token) =>
                (string.IsNullOrEmpty(SERVERURL) ? MY_IP : SERVERURL) +
                "/adtrack" +
                "/reservation/" + contract_token;
        }
    }
}
