using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Networking;

namespace HotPlay
{
    public class WebRequestAsyncOperation : INotifyCompletion
    {
        private UnityWebRequestAsyncOperation asyncOperation;
        private Action continuation;

        public WebRequestAsyncOperation(UnityWebRequestAsyncOperation asyncOperation)
        {
            this.asyncOperation = asyncOperation;
            asyncOperation.completed += OnRequestComplete;
        }

        public bool IsCompleted
        {
            get { return asyncOperation.isDone; }
        }

        public void GetResult()
        {

        }

        private void OnRequestComplete(AsyncOperation obj)
        {
            continuation();
        }

        public void OnCompleted(Action continuation)
        {
            this.continuation = continuation;
        }
    }

    public static class ExtensionMethod
    {
        public static WebRequestAsyncOperation GetAwaiter(this UnityWebRequestAsyncOperation asyncOperation)
        {
            return new WebRequestAsyncOperation(asyncOperation);
        }
    }
}