﻿using System;
using System.Threading.Tasks;
using HotPlay;
using UnityEngine;
using UnityEngine.Networking;

namespace HotPlay
{
    public class GenericWebRequest
    {
        public async Task<UnityWebRequest> Get(string url)
        {
            UnityWebRequest www = UnityWebRequest.Get(url);
            SetAuthorizationHeader(www);

            await www.SendWebRequest();

            return www;
        }

        public async Task<UnityWebRequest> Post(string url, WWWForm form)
        {
            UnityWebRequest www = UnityWebRequest.Post(url, form);
            SetAuthorizationHeader(www);

            await www.SendWebRequest();

            return www;
        }

        public async Task<UnityWebRequest> Put(string url, string json)
        {
            UnityWebRequest www = UnityWebRequest.Put(url, json);
            SetAuthorizationHeader(www);

            await www.SendWebRequest();

            return www;
        }

        public async Task<Texture2D> GetTexture(string url)
        {
            if (string.IsNullOrEmpty(url) == false)
            {
                var www = UnityWebRequestTexture.GetTexture(url);
                SetAuthorizationHeader(www);

                await www.SendWebRequest();

                return DownloadHandlerTexture.GetContent(www);
            }
            return null;
        }

        public WebRequestCodeAndMessage GetResponseFromRequest(UnityWebRequest request)
        {
            if (request == null) return null;

            var response = new WebRequestCodeAndMessage();
            response.code = request.responseCode;
            response.message = request.downloadHandler.text;
            return response;
        }

        private void SetAuthorizationHeader(UnityWebRequest webRequest)
        {
            string code = LoginManager.Instance.AccessToken;
            if (string.IsNullOrEmpty(code))
            {
                return;
            }
            webRequest.SetRequestHeader("Authorization", "Bearer " + code);
        }
    }
}