﻿namespace HotPlay
{
    public class WebRequestCodeAndMessage
    {
        public long code;
        public string message;

        public WebRequestCodeAndMessage()
        {
        }

        public WebRequestCodeAndMessage(long code, string message)
        {
            this.code = code;
            this.message = message;
        }
    }
}